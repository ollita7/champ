const path = require("path");
const webpack = require('webpack');
const dotenv = require('dotenv');

const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: "./static/index.tsx",
  output: { path: path.join(__dirname, "build"), filename: "index.bundle.js" },
  mode: process.env.NODE_ENV || "development",
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        resolve: {
          fullySpecified: false,
        },
      },
      {
        test: /\.(js|jsx|ts|tsx)$/,
        exclude: /node_modules[/\\](?!(sonic-ui))/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-env',
              '@babel/preset-react',
              [
                '@babel/preset-typescript',
                {
                  allExtensions: true,
                  isTSX: true,
                },
              ],
            ],
            plugins: [
              ['@babel/plugin-transform-flow-strip-types', { loose: true }],
              ['@babel/plugin-proposal-class-properties', { loose: true }],
              ['@babel/plugin-proposal-private-methods', { loose: true }],
              ['@babel/plugin-proposal-private-property-in-object', { loose: true }],
              ['@babel/plugin-transform-runtime', { loose: true }],
              [
                'module-resolver',
                {
                  root: ['./src'],
                  extensions: ['.tsx'],
                },
              ],
              ['inline-react-svg', { ignorePattern: /^(.(?!\.component\.svg$))+$/, caseSensitive: true }],
            ],
          },
        },
      },
      {
        test: /\.(css|scss)$/,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
      {
        test: /\.(jpg|jpeg|png|gif|mp3|svg|pdf)$/,
        use: ["file-loader"],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, "static", "index.html"),
    }),
    new webpack.ProvidePlugin({
      Buffer: ['buffer', 'Buffer'],
      process: 'process/browser',
      React: 'react',
    }),
    new webpack.DefinePlugin({
      'process.env': JSON.stringify(dotenv.config().parsed), // it will automatically pick up key values from .env file
    })
  ],
  devServer: {
    historyApiFallback: true,
  },
};