import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IMatch, IResult } from '../../network/services/tournament.service';

export enum FORM_TYPE {
  CREATE = 'CREATE',
  EDIT = 'EDIT'
}

export interface IMatchState {
  isFormVisible: boolean;
  typeForm: FORM_TYPE;
  current: IMatch | null;
}

const initialState: IMatchState = {
 isFormVisible: false,
 typeForm: FORM_TYPE.CREATE,
 current: null
}

const matchSlice = createSlice({
  name: 'match',
  initialState,
  reducers: {
    setIsFormVisible: (state, action: PayloadAction<boolean>) => {
      const newState = {...state, isFormVisible: action.payload };
      return newState;
    },
    setEdit: (state, action: PayloadAction<IMatchState>) => {
      const newState = {
          ...state, 
          isFormVisible: true,
          current: action.payload.current, 
          typeForm: FORM_TYPE.EDIT };
      return newState;
    },
    setCurrent: (state, action: PayloadAction<IResult>) => {
      if (state.current) state.current.result = action.payload;
    },
    clear: state => initialState,
  },
});

export default matchSlice.reducer;
export const { setIsFormVisible, setEdit, clear, setCurrent} = matchSlice.actions;