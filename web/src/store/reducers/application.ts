import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IMember } from '../../network/services/tournament.service';


export interface ICup {
  id: string;
  name: string;
}
export interface IApplicationState {
  event: string | null;
  sport_type: string | null;
  tournament: string | null;
  show_group: boolean;
  category: string | null;
  cups: Array<ICup> | null;
  stages: Array<string> | null;
  current_stage: string | null;
  members: Array<IMember> | null;
  needsUpdate: boolean;
}

const initialState: IApplicationState = {
  event: null,
  sport_type: null,
  tournament: null,
  category: null, 
  cups: null,
  stages: null,
  current_stage: null,
  needsUpdate: false,
  members: [],
  show_group: true
}

const applicationSlice = createSlice({
  name: 'application',
  initialState,
  reducers: {
    setApplication: (state, action: PayloadAction<IApplicationState>) => {
      const newState = {...state, ...action.payload };
      return newState;
    },
    setNeedsUpdate: (state, action: PayloadAction<boolean>) => {
      const newState = {...state, needsUpdate: action.payload };
      return newState;
    },
    clearApplication: state => initialState,
  },
});

export default applicationSlice.reducer;
export const { setApplication, clearApplication, setNeedsUpdate} = applicationSlice.actions;
