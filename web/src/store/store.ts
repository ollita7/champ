import { configureStore } from '@reduxjs/toolkit';
import profileReducer from './reducers/profile';
import applicationReducer from './reducers/application';
import matchReducer from './reducers/match';

const store = configureStore({
  reducer: {
    profile: profileReducer,
    application: applicationReducer,
    match: matchReducer
  },
});

export default store;
export type RootState = ReturnType<typeof store.getState>