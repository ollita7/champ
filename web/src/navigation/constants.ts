enum ROUTES {
  LOGIN = '/login',
  PAYMENT = '/payment',
  HOME = '/',
  MATCHES = '/matches',
  QUALIFYINGS = '/qualifyings',
  GROUP = '/qualifyings',
  SPONSORS = '/sponsors',
  EVENTS = '/events',
  RANKING = '/ranking',
  STATISTICS = '/statistics',
  TERMS = '/terms',
  WILDCARD = '*',
}

export {ROUTES}