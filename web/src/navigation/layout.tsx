import React, { useEffect, useState } from 'react';
import { Outlet } from 'react-router-dom';
import { connect } from "react-redux";
import { useNavigate, useLocation } from 'react-router-dom';
import ResponsiveMenu from '../components/menu/menu';
import { IStoreDispatchProps } from '../store/storeComponent';
import { getApplication, getProfile } from '../store/selectors';
import { RootState } from "../store/store";
import { IProfileState } from '../store/reducers/profile';
import MatchEdit  from '../components/matches/match/edit/edit';
import { IApplicationState } from '../store/reducers/application';
import { ROUTES } from './constants';

import CircularProgress from '@mui/material/CircularProgress';
import { Typography } from '@mui/material';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import FormatListNumberedIcon from '@mui/icons-material/FormatListNumbered';
import EmojiEventsIcon from '@mui/icons-material/EmojiEvents';
import MilitaryTechIcon from '@mui/icons-material/MilitaryTech';
import { setTheme } from '../utils/theme';
import { setTenant } from '../network/services/tenant.service';
import { useGetTenant } from '../network/services/tenant.service';
import CategoryIcon from '@mui/icons-material/Category';
import { MatchEditSoccer } from '../components/matches/match/edit';

interface ILayoutProps extends IStoreDispatchProps {
  profile: IProfileState;
  application: IApplicationState;
}

const MENU_ROUTES = [
  ROUTES.HOME,
  ROUTES.QUALIFYINGS,
  ROUTES.RANKING,
  ROUTES.EVENTS
]

const Layout = ({application, profile, ...props }: ILayoutProps): React.ReactElement => {
  const location = useLocation();
  const navigate = useNavigate();
  const [value, setValue] = useState(index);
  setTenant();
  const { data, isLoading, isSuccess }  = useGetTenant();
  
  const index = MENU_ROUTES.findIndex((item) => item === location.pathname);

  const handleChange = (event, path) => {
    setValue(index);
    navigate(path);
  }

  useEffect(() => {
    const index = MENU_ROUTES.findIndex((item) => item === location.pathname);
    setValue(index);
  }, [location.pathname])

  useEffect(() => {
    if (data) setTheme(document.documentElement, data);
  }, [isSuccess])

  return (
    <>
      {isLoading && 
        <div className='loading-app'>
          <CircularProgress/>
          <Typography>Cargando</Typography>
        </div>
      }
      {isSuccess && data &&
        <>
          <div className="layout">
            <ResponsiveMenu/>
            <Outlet />
            {application.sport_type === 'soccer' && <MatchEditSoccer/>}
            {application.sport_type === 'tennis' && <MatchEdit/>}
          </div>
          <BottomNavigation className='bottom-menu'
              showLabels
              value={value}
              onChange={handleChange}>
            {/*<BottomNavigationAction value={ROUTES.HOME} label="Grupos" icon={<GroupIcon />} />*/}
            <BottomNavigationAction value={ROUTES.EVENTS} label="Etapas" icon={<CategoryIcon />} />
            <BottomNavigationAction value={ROUTES.HOME} label="Cruces" icon={<EmojiEventsIcon />} />
            {isSuccess && data && data.ranking &&
              <BottomNavigationAction value={ROUTES.RANKING} label="Ranking" icon={<MilitaryTechIcon />} />
            }
            {application.sport_type == "soccer" &&
              <BottomNavigationAction value={ROUTES.STATISTICS} label="Estadisticas" icon={<FormatListNumberedIcon />} />
            }
          </BottomNavigation>
        </>
    }
    </>
  );
};

const mapStateToProps = (state: RootState) => ({
  profile: getProfile(state),
  application: getApplication(state)
});

export default connect(mapStateToProps)(Layout);