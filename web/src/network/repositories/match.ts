import { BaseClient } from '../appClient';

const ENDPOINTS = {
  addResultUrl: (match_id: string,) => `match/${match_id}/result`,
  editMatchUrl: (match_id: string,) => `match/${match_id}`,
  autoGenerateGroupMatchesUrl: () => `match/autogenerate`
} 

const addResult = (match_id: string, data): Promise<any> => {
  return BaseClient.put(ENDPOINTS.addResultUrl(match_id), data);
};

const editMatch = (match_id: string, data): Promise<any> => {
  return BaseClient.put(ENDPOINTS.editMatchUrl(match_id), data);
};

const autoGenerateGroupMatches = (data): Promise<any> => {
  return BaseClient.post(ENDPOINTS.autoGenerateGroupMatchesUrl(), data);
};

export const MatchRepository = {
  addResult, editMatch, autoGenerateGroupMatches
}