import { BaseClient } from '../appClient';

const ENDPOINTS = {
  getTournamentsUrl: () => `tournament`,
  getTournamentUrl: (tournament_id: string, category_id: string) => `tournament/${tournament_id}/category/${category_id}`,
  getMatchesByStageUrl: (tournament_id: string, category_id: string, stage: string, group: string) => `tournament/${tournament_id}/category/${category_id}/stage/${stage}/group/${group}`,
  getMembersUrl: (tournament_id: string, category_id: string) => `tournament/${tournament_id}/category/${category_id}/members`,
  getTournamentStatisticsUrl: (tournament_id: string, category_id: string) => `tournament/${tournament_id}/category/${category_id}/statistics`,
  getMatchesByTournamentUrl: (tournament_id: string, group: string) => `tournament/match/${tournament_id}/group/${group}`
} 

const getTournament = (parameters: any): Promise<any> => {
  return BaseClient.get(ENDPOINTS.getTournamentUrl(parameters.queryKey[1], parameters.queryKey[2]));
};

const getMatchesByStage = (parameters: any): Promise<any> => {
  return BaseClient.get(ENDPOINTS.getMatchesByStageUrl(parameters.queryKey[1], parameters.queryKey[2], parameters.queryKey[3], parameters.queryKey[4]));
};

const getMembers = (parameters: any): Promise<any> => {
  return BaseClient.get(ENDPOINTS.getMembersUrl(parameters.queryKey[1], parameters.queryKey[2]));
};

const getTournaments = (parameters: any): Promise<any> => {
  if (!parameters.queryKey[1])
    return BaseClient.get(ENDPOINTS.getTournamentsUrl());
  else
  return BaseClient.get(`${ENDPOINTS.getTournamentsUrl()}/${parameters.queryKey[1]}`);
};

const getTournamentStatistics = (parameters: any): Promise<any> => {
  return BaseClient.get(ENDPOINTS.getTournamentStatisticsUrl(parameters.queryKey[1], parameters.queryKey[2]));
};

const getMatchesByTournament = (parameters: any): Promise<any> => {
  return BaseClient.get(ENDPOINTS.getMatchesByTournamentUrl(parameters.queryKey[1], parameters.queryKey[2]));
};

export const TournamentRepository = {
  getTournament, getMembers, getMatchesByStage, getTournaments, getTournamentStatistics, getMatchesByTournament
}