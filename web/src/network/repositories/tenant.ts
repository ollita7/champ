import { BaseClient } from '../appClient';

const ENDPOINTS = {
  getUr: () => `tenant`
} 

const get = (): Promise<any> => {
  return BaseClient.get(ENDPOINTS.getUr());
};


export const TenantRepository = {
  get
}