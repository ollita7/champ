export const enum QUERIES_KEYS {
  USER_GET_PROFILE = 'user/get_profile',
  GET_TOURNAMENT = 'get_torunament',
  GET_TOURNAMENTS = 'get_torunaments',
  GET_MATCHES_BY_TOURNAMENT = 'get_matches_by_tournament',
  GET_TOURNAMENT_MEMBERS = 'get_torunament_members'
}