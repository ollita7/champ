import { useQuery, useMutation } from 'react-query';
import { TenantRepository } from "../repositories/tenant";
import {  QUERIES_KEYS } from '../queryKeys';

export interface ILogin {
  username?: string;
  password?: string;
}

const useGetTenant = (): {
  isLoading: boolean;
  isSuccess: boolean;
  isError: boolean;
  data: any;
  refetch: () => void;
} => {
  return useQuery([`${QUERIES_KEYS.USER_GET_PROFILE}`], TenantRepository.get, {
    enabled: true,
    select: (response) => {
      return response
    },
  });
}

const setTenant = () => {
  var host = window.location.host
  var subdomain = host.split('.')[0];
  localStorage.setItem('tenant', !subdomain || subdomain ==='localhost:8080' ? 'marak': subdomain);
}

const getTenant = (): string | null => {
  return localStorage.getItem('tenant');
}

export { useGetTenant, setTenant, getTenant}