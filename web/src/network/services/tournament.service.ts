import { useMutation, useQuery } from "react-query";
import { QUERIES_KEYS } from "../queryKeys";
import { TournamentRepository } from "../repositories/tournament";

export interface ITeam {
  id: string;
  name: string;
  country: string;
}

export interface ISet {
  games: number[];
  tie: boolean;
  tie_result?: number[] | null;
}

export interface IResult {
  set_1: ISet;
  set_2: ISet;
  super?: number[] | null;
  winner: number;
  wo?: number;
  ret?: number;
}

export interface IResultSoccer {
  goals_visit: number;
  goals_local: number;
  ret?: number;
}

export interface IMatch {
  category_id: string;
  date: Date | string;
  group: string;
  team_local: any;
  team_visit: any;
  name: string;
  tournament_id: string;
  stage: string;
  _id: string;
  result: IResult | IResultSoccer;
}

export interface IGroup {
  id: string;
  name: string;
  teams: ITeam[];
  matches: IMatch[];
}

export interface ICup {
  id: string;
  name: string;
}

export interface ITournament {
  _id: string;
  date: Date;
  name: string;
  category: string;
  groups: IGroup[];
}

export interface IEvent {
  id: string;
  name: string;
  date: Date;
  description: string;
  current: boolean;
  categories: ITournamentReduced[];
}

export interface ITournamentReduced {
  tournament_id: string;
  date: Date;
  category_id: string;
  category: string;
  stages: Array<string>;
  cups?: Array<ICup>;
  members: Array<any>;
}

export interface IMember {
  name: string;
  country?: string;
}

const userGetTournament = (
  tournament_id,
  category_id
): {
  isLoading: boolean;
  isSuccess: boolean;
  isError: boolean;
  data: any;
  refetch: () => void;
} => {
  return useQuery(
    [`${QUERIES_KEYS.GET_TOURNAMENT}`, tournament_id, category_id],
    TournamentRepository.getTournament,
    {
      enabled: !!tournament_id && !!category_id,
      select: (response: ITournament) => response,
    }
  );
};

const useGetMatchesByStage = (
  tournament_id,
  category_id,
  stage,
  cup
): {
  isLoading: boolean;
  isSuccess: boolean;
  isError: boolean;
  data: any;
  refetch: () => void;
} => {
  return useQuery(
    [`${QUERIES_KEYS.GET_TOURNAMENT}`, tournament_id, category_id, stage, cup],
    TournamentRepository.getMatchesByStage,
    {
      enabled: !!tournament_id && !!category_id && !!stage,
      select: (response: ITournament) => {
        return response;
      },
    }
  );
};

const useGetTournaments = (
  event_id?: string
): {
  isLoading: boolean;
  isSuccess: boolean;
  isError: boolean;
  data: any;
  refetch: () => void;
} => {
  return useQuery(
    [`${QUERIES_KEYS.GET_TOURNAMENTS}`, event_id],
    TournamentRepository.getTournaments,
    {
      select: (response: ITournament[]) => response,
    }
  );
};

const useGetMatchesByTournament = (tournament_id, cup) : {
  isLoading: boolean;
  isSuccess: boolean;
  isError: boolean;
  data: any;
  refetch: () => void;
} => {
  return useQuery(
    [`${QUERIES_KEYS.GET_MATCHES_BY_TOURNAMENT}`, tournament_id, cup],
    TournamentRepository.getMatchesByTournament,
    {
      enabled: !!tournament_id,
      select: (response: IMatch[]) => {
        return response;
      },
    }
  );
};

const useGetMembers = (
  tournament_id,
  category_id
): {
  isLoading: boolean;
  isSuccess: boolean;
  isError: boolean;
  data: any;
  refetch: () => void;
} => {
  return useQuery(
    [`${QUERIES_KEYS.GET_TOURNAMENT}`, tournament_id, category_id],
    TournamentRepository.getMembers,
    {
      enabled: !!tournament_id && !!category_id,
      select: (response: IMember) => response,
    }
  );
};

const userGetTournamentStatistics = (
  tournament_id,
  category_id
): {
  isLoading: boolean;
  isSuccess: boolean;
  isError: boolean;
  data: any;
  refetch: () => void;
} => {
  return useQuery(
    [`${QUERIES_KEYS.GET_TOURNAMENT}`, tournament_id, category_id],
    TournamentRepository.getTournamentStatistics,
    {
      enabled: !!tournament_id && !!category_id,
      select: (response: ITournament) => response,
    }
  );
};

export {
  userGetTournament,
  useGetMatchesByStage,
  useGetTournaments,
  useGetMembers,
  useGetMatchesByTournament,
  userGetTournamentStatistics,
};
