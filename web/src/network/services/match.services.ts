import { useQuery, useMutation } from 'react-query';
import { MatchRepository } from "../repositories/match";
import { Config } from '../../utils/config';
import {  QUERIES_KEYS } from '../queryKeys';
import { IGroup, IResult, IResultSoccer, ITeam } from './tournament.service';

export interface IStanding{
  pj: number;
  sf: number;
  gf: number;
  pts: number;
  member: any;
}

export interface IStandingSoccer{
  pj: number;
  pp: number;
  pe: number;
  pg: number;
  dif: number;
  pts: number;
  member: any;
}

export interface IAddResult {
  id: string;
  result: IResult | IResultSoccer;
}

export interface IAutogenerate {
  tournament_id: string;
  group: string;
}

export interface IAddDate {
  id: string;
  date: Date | string;
  team_local: any;
  team_visit: any;
}


const getStandings = (group: IGroup): Array<IStanding> => {
  const standings: Array<IStanding> = [];
  group?.teams.forEach((team: ITeam) => {
    const row: IStanding = {pj: 0, sf: 0, pts: 0, gf: 0, member: team}
    const result = group.matches
      .filter(gm => (gm.team_local.name === team.name || gm.team_visit.name === team.name ) && gm.result)
    const statistic = result
      .map(m => {
        const data = calculateData(m.team_local.name == team.name ? 0 : 1, m.result)
        row.sf += data.sf;
        row.gf += data.gf;
        row.pts += data.pts;
        row.member = team;
      });
    row.pj = result.length;
    standings.push(row);
  });
  return standings.sort((a, b) => b.pts - a.pts || b.sf - a.sf || b.gf - a.gf);
}

const getStandingsSoccer = (group: IGroup): Array<IStandingSoccer> => {
  const standings: Array<IStandingSoccer> = [];
  group?.teams.forEach((team: ITeam) => {
    const row: IStandingSoccer = {pj: 0, pp: 0, pe: 0, pg: 0, dif: 0, pts: 0, member: team}
    const result = group.matches
      .filter(gm => (gm.team_local.name === team.name || gm.team_visit.name === team.name ) && gm.result)
    const statistic = result
      .map(m => {
        const data = calculateDataSoccer(m.team_local.name == team.name ? 0 : 1, m.result)
        row.pj += 1;
        row.pg += data.pg;
        row.dif += data.dif;
        row.pe += data.pe
        row.pp += data.pp 
        row.pts += data.pts;
        row.member = team;
      });
    row.pj = result.length;
    standings.push(row);
  });
  return standings.sort((a, b) => b.pts - a.pts || b.dif - a.dif || b.pg - a.pg);
}

const calculateDataSoccer = (index, result)=>{
  const data = {
    pts: 0,
    pg: 0, 
    dif: 0,
    pe: 0,
    pp: 0
  }
  switch (index){
    case 0:
      data.pg =  result.goals_local > result.goals_visit ? 1: 0;
      data.dif = result.goals_local - result.goals_visit;
      data.pe = result.goals_local - result.goals_visit == 0 ? 1 : 0;
      data.pp = result.goals_local < result.goals_visit ? 1 : 0;
      data.pts = result.goals_local > result.goals_visit ? 3 : result.goals_local < result.goals_visit ? 0 : 1;
      break;
    case 1:
      data.pg =  result.goals_local < result.goals_visit ? 1: 0;
      data.dif = result.goals_visit - result.goals_local;
      data.pe = result.goals_local - result.goals_visit == 0 ? 1 : 0;
      data.pp = result.goals_visit < result.goals_local ? 1 : 0;
      data.pts = result.goals_visit > result.goals_local ? 3 : result.goals_visit < result.goals_local ? 0 : 1;
      break;
  }
  return data;
}

const calculateData = (index, result): any => {
  const data = {
    pts: result.winner == index,
    sf: 0, 
    gf: 0
  }
  switch (index){
    case 0:
      data.sf += result.set_1.games[0] > result.set_1.games[1] ? 1 : -1;
      if(result.set_2.games[0] + result.set_2.games[1] > 5){
        data.sf += result.set_2.games[0] > result.set_2.games[1] ? 1 : -1;
      }
      
      break;
    case 1: 
      data.sf += result.set_1.games[0] < result.set_1.games[1] ? 1 : -1;
      if(result.set_2.games[0] + result.set_2.games[1] > 5){
        data.sf += result.set_2.games[0] < result.set_2.games[1] ? 1 : -1;
      }
      break;
  }
  data.gf += result.set_1.games[index] - result.set_1.games[index ? 0: 1];
  if(result.set_2.games[0] + result.set_2.games[1] > 5){
    data.gf += result.set_2.games[index] - result.set_2.games[index ? 0 : 1];
  }
  
  data.sf += result.super? data.pts ? 1: -1 : 0;
  return data;
}

const useAddResult = () => {
  const mutation = useMutation((data: IAddResult) => {
    return MatchRepository.addResult(data.id, data.result);
  });
  return mutation;
};

const useAutoGenerateGroupMatches = () => {
  const mutation = useMutation((data: IAutogenerate) => {
    return MatchRepository.autoGenerateGroupMatches(data);
  });
  return mutation;
};

const useEditMatch = () => {
  const mutation = useMutation((data: IAddDate) => {
    return MatchRepository.editMatch(data.id, data);
  });
  return mutation;
};

export {getStandings, useAddResult, useEditMatch, useAutoGenerateGroupMatches, getStandingsSoccer}