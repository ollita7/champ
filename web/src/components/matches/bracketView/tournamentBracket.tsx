import { useEffect, useRef, useState } from "react";
import { Bracket, Seed, SeedItem } from "react-brackets";
import { Match } from "../../../components/matches/match";
import { IMatch } from "../../../network/services/tournament.service";

import "./styles.scss";
import { useMediaQuery } from "@mui/material";
export const TournamentBracket: React.FC<{
  matches: IMatch[];
  selectedStage: string;
  setSelectedStage: (stage: string) => void;
}> = ({ matches, selectedStage, setSelectedStage }) => {
  const isMobile = useMediaQuery("(max-width: 790px)");

  const groupedMatches = matches.reduce((acc, match) => {
    if (!match.stage || match.stage === "group") return acc;
    const stage = match.stage;
    if (!acc[stage]) {
      acc[stage] = [];
    }
    acc[stage].push(match);
    return acc;
  }, {} as Record<string, IMatch[]>);

  const validStages = ["16F", "QF", "SF", "F"];
  const sortedStages = Object.keys(groupedMatches)
    .filter((stage) => validStages.includes(stage))
    .sort((a, b) => validStages.indexOf(a) - validStages.indexOf(b));

  const filteredMatches = isMobile
    ? groupedMatches[selectedStage] || []
    : matches;

  const rounds = isMobile
    ? [
        {
          title: "",
          seeds: filteredMatches.map((match) => ({
            id: match._id,
            date: match.date.toString(),
            teams: [
              { name: match.team_local.name },
              { name: match.team_visit.name },
            ],
            match,
          })),
        },
      ]
    : sortedStages.map((stage) => ({
        title: stage,
        seeds: groupedMatches[stage].map((match) => ({
          id: match._id,
          date: match.date.toString(),
          teams: [
            { name: match.team_local.name },
            { name: match.team_visit.name },
          ],
          match,
        })),
      }));

  const renderSeedComponent = ({ seed }: { seed: any }) => (
    <Seed className="custom-seed">
      <SeedItem>
        <Match match={seed.match} />
      </SeedItem>
    </Seed>
  );

  return (
    <div className="tournament-bracket-container">
      <Bracket
        rounds={rounds}
        roundClassName="custom-round"
        renderSeedComponent={renderSeedComponent}
      />
    </div>
  );
};
