import React, { ReactElement, useState } from "react";
import { connect } from "react-redux";

import Typography from "@mui/material/Typography";
import { CircleFlag } from "react-circle-flags";
import Chip from "@mui/material/Chip";
import Paper from "@mui/material/Paper";
import { styled } from "@mui/material/styles";
import EmojiEventsIcon from "@mui/icons-material/EmojiEvents";
import CheckIcon from "@mui/icons-material/Check";
import EditIcon from "@mui/icons-material/Edit";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";

//TODO: import { getMember } from '../../../network/services/match.services';
import { IMatch } from "../../../network/services/tournament.service";
import { getApplication, getMatch, getProfile } from "../../../store/selectors";
import { RootState } from "../../../store/store";
import { IProfileState } from "../../../store/reducers/profile";
import { IMatchState } from "../../../store/reducers/match";
import { IStoreDispatchProps } from "../../../store/storeComponent";
import { setEdit } from "../../../store/reducers/match";

import "./styles.scss";
import EditMatch from "./EditMatch";
import { IApplicationState } from "../../../store/reducers/application";

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  textAlign: "center",
  color: theme.palette.text.secondary,
  paddingTop: 10,
  paddingBottom: 10,
}));

export interface IMatchesProps extends IStoreDispatchProps {
  match: IMatch;
  profile: IProfileState;
  matchState: IMatchState;
  application: IApplicationState;
}

const Match: React.FC<IMatchesProps> = ({
  profile,
  match,
  matchState,
  application,
  ...props
}): ReactElement => {
  const [editMode, setEditMode] = useState<boolean>(false);

  const renderResult = (result, player, bye) => {
    if (result) {
      const winner = result.winner == player;
      return (
        <div className="result">
          {winner && (
            <div className="check">
              <CheckIcon />
            </div>
          )}
          <Typography className="wo">
            {result.wo === player && result.winner !== player && (
              <Chip size="small" label="WO" />
            )}
          </Typography>
          <Typography className="wo">
            {result.ret === player && result.winner !== player && (
              <Chip size="small" label="RET" />
            )}
          </Typography>
          {(result.wo == -1 || !result.wo) && (
            <>
              <div
                className={
                  result.set_1.games[player] > result.set_1.games[1 - player] ||
                  bye
                    ? "set winner"
                    : "set"
                }
              >
                {result.set_1.games[player]}
                {result.set_1.tie && (
                  <sup>{result.set_1.tie_result[player]}</sup>
                )}
              </div>
              {(result.set_2.games[player] > 5 ||
                result.set_2.games[1 - player] > 5) && (
                <>
                  <div
                    className={
                      result.set_2.games[player] >
                        result.set_2.games[1 - player] || bye
                        ? "set winner"
                        : "set"
                    }
                  >
                    {result.set_2.games[player]}
                    {result.set_2.tie && (
                      <sup>{result.set_2.tie_result[player]}</sup>
                    )}
                  </div>
                  {result.super && (
                    <div className={winner ? "set winner" : "set"}>
                      {result.super[player]}
                    </div>
                  )}
                </>
              )}
            </>
          )}
        </div>
      );
    } else if (bye) {
      return (
        <div className="result">
          <div style={{ paddingRight: 20 }} className="check">
            <CheckIcon />
          </div>
        </div>
      );
    }
  };

  const RenderResultSoccer = (result, player) => {
    if (result) {
      let winner = -1;
      if (result.goals_local > result.goals_visit) winner = 0;
      else if (result.goals_local < result.goals_visit) winner = 1;
      else if (result.goals_local == result.goals_visit && result.penalties) {
        if (result.penalties.goals_local > result.penalties.goals_visit)
          winner = 0;
        else if (result.penalties.goals_local < result.penalties.goals_visit)
          winner = 1;
      }
      return (
        <div className="result">
          {/*winner == player && <div className='check'><CheckIcon /></div>*/}
          <div className={winner == player ? "set winner" : "set"}>
            {player == 0 && (
              <>
                {result.goals_local}{" "}
                {result.goals_local == result.goals_visit &&
                  result.penalties && <>({result.penalties.goals_local})</>}
              </>
            )}
            {player == 1 && (
              <>
                {result.goals_visit}{" "}
                {result.goals_local == result.goals_visit &&
                  result.penalties && <>({result.penalties.goals_visit})</>}
              </>
            )}
          </div>
        </div>
      );
    }
  };

  const handleEdit = (renderDatePicker) => {
    if (!match.result) {
      if (application.sport_type == "tennis") {
        match.result = {
          set_1: {
            games: ["", ""],
            tie: false,
          },
          set_2: {
            games: ["", ""],
            tie: false,
          },
          winner: -1,
        };
      } else {
        match.result = {
          goals_local: 0,
          goals_visit: 0,
        };
      }
    }
    const newState = { ...matchState, current: match };
    props.dispatch(setEdit(newState));
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEditMode(event.target.checked);
  };

  const isWinner = (player: number, match: IMatch) => {
    if (application.sport_type == "tennis") {
      if (
        player == 0 &&
        (match.team_visit.name == "BYE" ||
          (match.result && !match.result.winner))
      )
        return "winner";
      if (
        player == 1 &&
        (match.team_local.name == "BYE" ||
          (match.result && match.result.winner))
      )
        return "winner";
    } else if (application.sport_type == "soccer" && match.result) {
      if (
        match.result.goals_local == match.result.goals_visit &&
        match.result.penalties
      ) {
        if (
          player == 0 &&
          match.result.penalties?.goals_local >
            match.result.penalties?.goals_visit
        ) {
          return "winner";
        }
        if (
          player == 1 &&
          match.result.penalties?.goals_visit >
            match.result.penalties?.goals_local
        ) {
          return "winner";
        }
      } else {
        if (
          player == 0 &&
          match.result.goals_local > match.result.goals_visit
        ) {
          return "winner";
        }
        if (
          player == 1 &&
          match.result.goals_visit > match.result.goals_local
        ) {
          return "winner";
        }
      }
    }
    return "";
  };

  const renderMatch = (match: IMatch, disable = false) => {
    //TODO:
    const member_1 = { country: match.team_local.country || "uy" };
    const member_2 = { country: match.team_visit.country || "uy" };

    return (
      <div
        className={`${disable ? "match" : "match"} ${
          match.stage === "group" ? "group-match" : ""
        }`}
        key={match._id}
      >
        <Item elevation={4}>
          <div className="details">
            {match.group == "GOLD" && (
              <div style={{ color: "#FFD700" }}>
                <EmojiEventsIcon />{" "}
              </div>
            )}
            {match.group == "SILVER" && (
              <div style={{ color: "#B5B7BB" }}>
                <EmojiEventsIcon />{" "}
              </div>
            )}
            {match.group != "GOLD" && match.group !== "SILVER" && (
              <span className="title">
                <Typography fontSize={11}>{match.group}</Typography>
              </span>
            )}
            {!editMode && (
              <span className="date">
                <Typography>
                  {match.date === "TBD" ? (
                    <strong>{match.date}</strong>
                  ) : (
                    new Date(match.date).toLocaleDateString("es-es", {
                      weekday: "long",
                      month: "long",
                      day: "numeric",
                      hour: "2-digit",
                      minute: "2-digit",
                    })
                  )}
                </Typography>
              </span>
            )}
            {profile?._id && (
              <div className="edit" onClick={handleEdit}>
                <EditIcon />
              </div>
            )}
          </div>
          {profile?._id && (
            <div className="edit-mode">
              <FormGroup>
                <FormControlLabel
                  label="Editar"
                  control={
                    <Switch checked={editMode} onChange={handleChange} />
                  }
                />
              </FormGroup>
            </div>
          )}
          {profile?._id && editMode ? (
            <EditMatch match={match} />
          ) : (
            <>
              <div className="player">
                <div className="img">
                  {member_1.country.toLowerCase() != "bye" &&
                    match.team_local.name?.toLowerCase() != "tbd" && (
                      <CircleFlag
                        countryCode={member_1.country.toLowerCase()}
                        height="25"
                      />
                    )}
                </div>
                <Typography>
                  <span className={`member ${isWinner(0, match)}`}>
                    {match.team_local.name.toUpperCase()}
                  </span>
                </Typography>
                {application.sport_type == "tennis" &&
                  renderResult(match.result, 0, match.team_visit.name == "BYE")}
                {application.sport_type == "soccer" &&
                  RenderResultSoccer(match.result, 0)}
              </div>
              <div className="player">
                <div className="img">
                  {member_2.country.toLowerCase() != "bye" &&
                    match.team_visit.name?.toLowerCase() != "tbd" && (
                      <CircleFlag
                        countryCode={member_2.country.toLowerCase()}
                        height="25"
                      />
                    )}
                </div>
                <Typography>
                  <span className={`member ${isWinner(1, match)}`}>
                    {match.team_visit.name.toUpperCase()}
                  </span>
                </Typography>
                {application.sport_type == "tennis" &&
                  renderResult(match.result, 1, match.team_local.name == "BYE")}
                {application.sport_type == "soccer" &&
                  RenderResultSoccer(match.result, 1)}
              </div>
            </>
          )}
        </Item>
      </div>
    );
  };

  return <>{renderMatch(match, match.date == "TBD")}</>;
};

const mapStateToProps = (state: RootState) => ({
  profile: getProfile(state),
  matchState: getMatch(state),
  application: getApplication(state),
});

export default connect(mapStateToProps)(Match);
