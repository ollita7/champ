import React, { ReactElement, useState } from 'react';
import { connect } from "react-redux";

import Button from '@mui/material/Button';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import SaveIcon from '@mui/icons-material/Save';
import ClearIcon from '@mui/icons-material/Clear';
import TextField from '@mui/material/TextField';
import dayjs, { Dayjs } from 'dayjs';
import "dayjs/locale/es";
import { MobileDateTimePicker } from '@mui/x-date-pickers/MobileDateTimePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';

import { RootState } from "../../../store/store";
import { IStoreDispatchProps } from '../../../store/storeComponent';
import { IAddDate, useEditMatch } from '../../../network/services/match.services';
import { getMatch, getApplication } from '../../../store/selectors';
import { setNeedsUpdate, IApplicationState } from '../../../store/reducers/application';
import { IMatch } from '../../../network/services/tournament.service';
import './styles.scss'

export interface IEditMatchProps extends IStoreDispatchProps {
  match: IMatch;
  application: IApplicationState
}

const EditMatch: React.FC<IEditMatchProps> = ({ application, match, ...props }): ReactElement => {
  const [date, setDate] = useState(match.date);
  const [team_local, setTeamLocal] = useState(match.team_local.name || 'TBD');
  const [team_visit, setTeamVisit] = useState(match.team_visit.name || 'TBD');

  const mutation = useEditMatch();

  const handleChangeDate = (newValue: Dayjs | null) => {
    setDate(newValue);
  }

  const handleSave = () => {
    //const d = date?.split('/');
    //const parsedDate = !d || d.length != 3 ? null : parseDate(`${d[1]}/${d[0]}/${d[2]} ${time}`);
    const data: IAddDate = {
      id: match._id, 
      date: date || 'TBD',
      team_local: application.members?.find(m => team_local == m.name),
      team_visit: application.members?.find(m => team_visit == m.name)
    };

    mutation.mutate(data, {
      onSuccess: async (response) => {
        props.dispatch(setNeedsUpdate(true));
      },
    });
  }

  const handleClear = () => {
    const data: IAddDate = {
      id: match._id, 
      date: 'TBD',
      team_local: application.members?.find(m => team_local == m.name),
      team_visit: application.members?.find(m => team_visit == m.name)
    };

    mutation.mutate(data, {
      onSuccess: async (response) => {
        setDate('TBD');
        props.dispatch(setNeedsUpdate(true));
      },
    });
  }

  return (
    <div className='edit-match'>
      <div className='date-section'>
        <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="es">
          <MobileDateTimePicker
            label="Fecha y hora"
            value={date}
            onChange={handleChangeDate}
            renderInput={(params) => <TextField {...params} />}
          />
        </LocalizationProvider>
        <SaveIcon onClick={handleSave}/>
        <ClearIcon onClick={handleClear}/>
      </div>
      <div className='player'>
        <FormControl variant="filled" fullWidth >
          <InputLabel id="demo-simple-select-label">Jugador 1</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={team_local}
            label="Jugador 1"
            disabled = {match.stage =='group'}
            onChange={(event) => {setTeamLocal(event.target.value)}}>
              <MenuItem value="TBD">TBD</MenuItem>
              {application?.members && application?.members.map(t => 
                <MenuItem value={t.name}>{t.name.toUpperCase()}</MenuItem>
              )}
          </Select>
        </FormControl>
      </div>
      <div className='player'>
        <FormControl variant="filled" fullWidth >
          <InputLabel id="demo-simple-select-label">Jugador 2</InputLabel>
          <Select
            disabled = {match.stage =='group'}
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={team_visit}
            label="Jugador 2"
            onChange={(event) => {setTeamVisit(event.target.value)}}>
              <MenuItem value="TBD">TBD</MenuItem>
              {application?.members && application.members.map(t => 
                <MenuItem value={t.name}>{t.name.toUpperCase()}</MenuItem>
              )}
          </Select>
        </FormControl>
      </div>
      <div className='buttons'>
        <div>
          <Button disabled = {match.stage =='group'} variant="contained" onClick={handleSave}>Guardar</Button>
        </div>
      </div>
    </div>
    
  )
}

const mapStateToProps = (state: RootState) => ({
  matchState: getMatch(state),
  application: getApplication(state)
});

export default connect(mapStateToProps)(EditMatch);


