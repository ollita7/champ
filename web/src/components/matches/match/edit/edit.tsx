import React, { ReactElement, useEffect, useState } from 'react';
import { connect } from "react-redux";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { Form, Formik, useFormikContext } from "formik";
import FormControlLabel from '@mui/material/FormControlLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Checkbox from '@mui/material/Checkbox';
import * as Yup from "yup";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import InputLabel from '@mui/material/InputLabel';
import { useTheme } from '@mui/material/styles';
import { Typography } from '@mui/material';

import { getMatch } from '../../../../store/selectors';
import { RootState } from "../../../../store/store";
import { IMatchState, setIsFormVisible } from '../../../../store/reducers/match';
import { setNeedsUpdate } from '../../../../store/reducers/application';
import { IStoreDispatchProps } from '../../../../store/storeComponent';
import { IAddResult, useAddResult } from '../../../../network/services/match.services';

import './styles.scss'

export interface DialogTitleProps {
  id: string;
  children?: React.ReactNode;
  onClose: () => void;
}

function BootstrapDialogTitle(props: DialogTitleProps) {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other} className='header'>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: 'var(--details)'
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

export interface IMatchesProps extends IStoreDispatchProps {
  matchState: IMatchState
}

const MatchEdit: React.FC<IMatchesProps> = ({matchState, ...props }): ReactElement => {
  const mutation = useAddResult();
  const players = ['', matchState.current?.team_local, matchState.current?.team_visit]

  const handleClose = () => {
    props.dispatch(setIsFormVisible(false));
  };

  const isVisibleTie1 = (values) =>{
    if (values.set_1_local + values.set_1_visit == 13)
      return true;
    return false;
  }

  const isVisibleTie2 = (values) =>{
    if (values.set_2_local + values.set_2_visit == 13)
      return true;
    return false;
  }

  const isVisibleSuper = (values) =>{
    if ((values.set_1_local > values.set_1_visit && values.set_2_local < values.set_2_visit) ||
      (values.set_1_local < values.set_1_visit && values.set_2_local > values.set_2_visit))
      return true;
    return false;
  }

  const calculateWinner = (values) => {
    if(values.wo >= 1)
      return values.wo == 1 ? 1: 0;
    if(values.ret >= 1)
      return values.ret == 1 ? 1: 0;
    const set_1 = values.set_1_local > values.set_1_visit ? 0 : 1; 
    if(values.set_2_local == 0 && values.set_2_visit == 0) return set_1;
    const set_2 = values.set_2_local > values.set_2_visit ? 0 : 1; 
    if (set_1 == set_2) return set_2;
    return values.super_local > values.super_visit ? 0 : 1
  }

  const handleSave = (values) => {
    if(values.ret ==-1 && values.wo == -1 && values.set_1_local == 0 && values.set_1_visit == 0 && values.set_2_local == 0 && values.set_2_visit == 0) return;
    const whoWin = calculateWinner(values);
    if (matchState.current){
      const newResult = {
        set_1: {
          games: [values.set_1_local, values.set_1_visit],
          tie: values.set_1_local_tie > 0 || values.set_1_visit_tie > 0,
          tie_result: [values.set_1_local_tie, values.set_1_visit_tie]
        },
        set_2:{
          games: [values.set_2_local, values.set_2_visit],
          tie: values.set_2_local_tie > 0 || values.set_2_visit_tie > 0,
          tie_result: [values.set_2_local_tie, values.set_2_visit_tie]
        },
        super: values.super_local> 0 || values.super_visit > 0 ?[values.super_local, values.super_visit] : null,
        winner: whoWin,
        wo: values.wo < 0 ? -1 : values.wo-1,
        ret: values.ret < 0 ? -1 : values.ret-1
      }
      const data: IAddResult = {id: matchState.current?._id, result: newResult};

      mutation.mutate(data, {
        onSuccess: async (response) => {
          props.dispatch(setNeedsUpdate(true));
          handleClose();
        },
      });
    }
  }

  let initialValues = {
    set_1_visit: undefined,
    set_1_local: undefined,
    set_1_tie: false,
    set_1_local_tie: undefined,  
    set_1_visit_tie: undefined,
    set_2_visit: undefined,
    set_2_local: undefined,
    set_2_tie: false,
    set_2_local_tie: undefined,  
    set_2_visit_tie: undefined,
    super_local: undefined,
    super_visit: undefined,  
    wo: -1,
    ret: -1
  };

  if (matchState.current?.result) 
    initialValues = {
      set_1_visit: matchState.current?.result.set_1.games[1],
      set_1_local: matchState.current?.result.set_1.games[0],
      set_1_tie: matchState.current?.result.set_1.tie,
      set_1_local_tie: matchState.current?.result.set_1.tie && matchState.current?.result.set_1.tie_result ? matchState.current?.result.set_1.tie_result[0]: undefined,  
      set_1_visit_tie: matchState.current?.result.set_1.tie && matchState.current?.result.set_1.tie_result ? matchState.current?.result.set_1.tie_result[1] : undefined,
      set_2_visit: matchState.current?.result.set_2.games[1],
      set_2_local: matchState.current?.result.set_2.games[0],
      set_2_tie: matchState.current?.result.set_2.tie,
      set_2_local_tie: matchState.current?.result.set_2.tie && matchState.current?.result.set_2.tie_result ? matchState.current?.result.set_2.tie_result[0] : undefined,  
      set_2_visit_tie: matchState.current?.result.set_2.tie && matchState.current?.result.set_2.tie_result ? matchState.current?.result.set_2.tie_result[1] : undefined,
      super_local: matchState.current?.result.super ? matchState.current?.result.super[0]: undefined,
      super_visit: matchState.current?.result.super ? matchState.current?.result.super[1]: undefined,
      wo: matchState.current?.result.wo >=0 ? matchState.current?.result.wo+1: -1, 
      ret: matchState.current?.result.ret >=0 ? matchState.current?.result.ret+1: -1,  
    };

  const wo_validation = {
    is: (wo, ret) => wo == -1 && ret == -1,
    then: Yup.number().required(``)
  }

  const ret_validation = {
    is: (wo, ret) => wo == -1 && ret == -1,
    then: Yup.number().required(``)
  }

  const loginSchema = Yup.object().shape({
    set_1_visit: Yup.number().when(['wo'], wo_validation),
    set_1_local: Yup.number().when(['wo'], wo_validation),
    set_1_tie: Yup.boolean(),
    set_1_local_tie: Yup.number(),
    set_1_visit_tie: Yup.number(),
    set_2_visit: Yup.number().when(['wo'], wo_validation),
    set_2_local: Yup.number().when(['wo'], wo_validation),
    set_2_tie: Yup.boolean(),
    set_2_local_tie: Yup.number(),
    set_2_visit_tie: Yup.number(),
    super_local: Yup.number(),
    super_visit: Yup.number(),
    wo: Yup.number(),
    ret: Yup.number()
  });

  const handleChangeRetWO = (setFieldValue, field) => {
    const value = field.target.value;
    const name = field.target.name;
    switch(name){
      case 'wo':
        setFieldValue('wo', value, true)
        if(value >= 0)
          setFieldValue('ret', -1, true);
      break;
      case 'ret':
        setFieldValue('ret', value, true)
        if(value >= 0)
          setFieldValue('wo', -1, true);
      break;
    }
  }

  const theme = useTheme();

  return (<div className='edit'>
            <Dialog open={matchState.isFormVisible} onClose={handleClose} fullScreen>
              <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}><Typography>Editar Partido</Typography></BootstrapDialogTitle>
              <DialogContent>
                <Formik 
                  initialValues={initialValues} 
                  validationSchema={loginSchema} 
                  onSubmit={(values) => {handleSave(values);}}>
                  {
                    ({ handleBlur, errors, touched, dirty, values, handleChange, setFieldValue }) => (
                    <Form className='form' >
                      <div className='team'>
                        <div className='name'><Typography>{matchState.current?.team_local.name}</Typography></div>
                        <div className='set'>
                          <TextField 
                            id="set_1_local" 
                            label="Set 1" 
                            type="number"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.set_1_local}
                            variant="standard"
                            helperText={touched.set_1_local ? errors.set_1_local : ""}
                            error={touched.set_1_local && Boolean(errors.set_1_local)}/>
                        </div>
                        {isVisibleTie1(values) && <div className='set tie'>
                          <TextField 
                            id="set_1_local_tie" 
                            label="Tie" 
                            type="number"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.set_1_local_tie}
                            variant="standard"
                            helperText={touched.set_1_local_tie ? errors.set_1_local_tie : ""}
                            error={touched.set_1_local_tie && Boolean(errors.set_1_local_tie)}/>
                        </div>}
                        <div className='set'>
                          <TextField 
                            id="set_2_local" 
                            label="Set 2" 
                            type="number"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.set_2_local}
                            variant="standard"
                            helperText={touched.set_2_visit ? errors.set_2_visit : ""}
                            error={touched.set_2_visit && Boolean(errors.set_2_visit)}/>
                        </div>
                        {isVisibleTie2(values) &&<div className='set tie'>
                          <TextField 
                            id="set_2_local_tie" 
                            label="Tie" 
                            type="number"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.set_2_local_tie}
                            variant="standard"
                            InputProps={{ inputProps: { min: 0} }}
                            helperText={touched.set_2_local_tie ? errors.set_2_local_tie : ""}
                            error={touched.set_2_local_tie && Boolean(errors.set_2_local_tie)}/>
                        </div>}
                        {isVisibleSuper(values) && <div className='set'>
                          <TextField 
                            id="super_local" 
                            label="Super" 
                            type="number"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.super_local}
                            variant="standard"
                            helperText={touched.super_local ? errors.super_local : ""}
                            error={touched.super_local && Boolean(errors.super_local)}/>
                        </div>}
                      </div>
                      <div className='team'>
                        <div className='name'><Typography>{matchState.current?.team_visit.name}</Typography></div>
                        <div className='set'>
                          <TextField 
                            id="set_1_visit" 
                            label="Set 1" 
                            type="number"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            variant="standard"
                            value={values.set_1_visit}
                            helperText={touched.set_1_visit ? errors.set_1_visit : ""}
                            error={touched.set_1_visit && Boolean(errors.set_1_visit)}/>
                        </div>
                        {isVisibleTie1(values) &&<div className='set tie'>
                          <TextField 
                            id="set_1_visit_tie" 
                            label="Tie" 
                            type="number"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.set_1_visit_tie}
                            variant="standard"
                            helperText={touched.set_1_visit_tie ? errors.set_1_visit_tie : ""}
                            error={touched.set_1_visit_tie && Boolean(errors.set_1_visit_tie)}/>
                        </div>}
                        <div className='set'>
                          <TextField 
                            id="set_2_visit" 
                            label="Set 2" 
                            type="number"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            variant="standard"
                            value={values.set_2_visit}
                            helperText={touched.set_2_visit ? errors.set_2_visit : ""}
                            error={touched.set_2_visit && Boolean(errors.set_2_visit)}/>
                        </div>
                        {isVisibleTie2(values) &&<div className='set tie'>
                          <TextField 
                            id="set_2_visit_tie" 
                            label="Tie" 
                            type="number"
                            value={values.set_2_visit_tie}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            InputProps={{ inputProps: { min: 0 } }}
                            variant="standard"
                            helperText={touched.set_2_visit_tie ? errors.set_2_visit_tie : ""}
                            error={touched.set_2_visit_tie && Boolean(errors.set_2_visit_tie)}/>
                        </div>}
                        {isVisibleSuper(values) && <div className='set'>
                          <TextField 
                            id="super_visit" 
                            label="Super" 
                            type="number"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.super_visit}
                            variant="standard"
                            helperText={touched.super_visit ? errors.super_visit : ""}
                            error={touched.super_visit && Boolean(errors.super_visit)}/>
                        </div>}
                      </div>
                      <div className='wo'>
                        <FormControl variant="filled" fullWidth >
                          <InputLabel id="demo-simple-select-label">Hay retiro? </InputLabel>
                          <Select
                            labelId="demo-simple-select-label"
                            name="ret"
                            value={values.ret > 0 ? values.ret : ''}
                            onChange={(value) => handleChangeRetWO(setFieldValue, value)}>
                              <MenuItem value={-1}></MenuItem>
                              {players.map((t, index) => 
                                <MenuItem value={index}>{t.name}</MenuItem>
                              )}
                          </Select>
                        </FormControl>
                      </div>
                      <div className='wo'>
                        <FormControl variant="filled" fullWidth >
                          <InputLabel id="demo-simple-select-label">Hay WO? </InputLabel>
                          <Select
                            labelId="demo-simple-select-label"
                            name="wo"
                            value={values.wo > 0 ? values.wo : ''}
                            onChange={(value) => handleChangeRetWO(setFieldValue, value)}>
                              <MenuItem value={-1}></MenuItem>
                              {players.map((t, index)=> 
                                <MenuItem value={index}>{t.name}</MenuItem>
                              )}
                          </Select>
                        </FormControl>
                      </div>
                      <div className='buttons'>
                        <div>
                          <Button variant="contained" type="submit">Guardar</Button>
                        </div>
                      </div>
                      
                    </Form>
                  )}
                </Formik>
                
              </DialogContent>
              <DialogActions>
                
              </DialogActions>
            </Dialog>
        </div>);
}

const mapStateToProps = (state: RootState) => ({
  matchState: getMatch(state)
});

export default connect(mapStateToProps)(MatchEdit);