import React, { ReactElement, useEffect, useState } from 'react';
import { connect } from "react-redux";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { Form, Formik, useFormikContext } from "formik";
import FormControlLabel from '@mui/material/FormControlLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Checkbox from '@mui/material/Checkbox';
import * as Yup from "yup";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import InputLabel from '@mui/material/InputLabel';
import { useTheme } from '@mui/material/styles';
import { Typography } from '@mui/material';

import { getMatch } from '../../../../store/selectors';
import { RootState } from "../../../../store/store";
import { IMatchState, setIsFormVisible } from '../../../../store/reducers/match';
import { setNeedsUpdate } from '../../../../store/reducers/application';
import { IStoreDispatchProps } from '../../../../store/storeComponent';
import { IAddResult, useAddResult } from '../../../../network/services/match.services';

import './styles.scss'

export interface DialogTitleProps {
  id: string;
  children?: React.ReactNode;
  onClose: () => void;
}

function BootstrapDialogTitle(props: DialogTitleProps) {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other} className='header'>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: 'var(--details)'
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

export interface IMatchesProps extends IStoreDispatchProps {
  matchState: IMatchState
}

const MatchEditSoccer: React.FC<IMatchesProps> = ({matchState, ...props }): ReactElement => {
  const mutation = useAddResult();
  const players = ['', matchState.current?.team_local, matchState.current?.team_visit]

  const handleClose = () => {
    props.dispatch(setIsFormVisible(false));
  };

  const calculateWinner = (values) => {
    if(values.ret >= 1)
      return values.ret == 1 ? 1: 0;
    if (values.goals_local > values.goals_visit)
      return 0;
    if (values.goals_local < values.goals_visit)
      return 1;
    return -1;
  }

  const handleSave = (values) => {
    if(values.ret ==-1 && values.wo == -1 && values.set_1_local == 0 && values.set_1_visit == 0 && values.set_2_local == 0 && values.set_2_visit == 0) return;
    const whoWin = calculateWinner(values);
    if (matchState.current){
      const newResult = {
        goals_local: values.goals_local,
        goals_visit: values.goals_visit,
        ret: values.ret < 0 ? -1 : values.ret-1
      }
      const data: IAddResult = {id: matchState.current?._id, result: newResult};

      mutation.mutate(data, {
        onSuccess: async (response) => {
          props.dispatch(setNeedsUpdate(true));
          handleClose();
        },
      });
    }
  }

  let initialValues = {
    goals_visit: undefined,
    goals_local: undefined,
    ret: -1
  };

  if (matchState.current?.result) 
    initialValues = {
      goals_visit: matchState.current?.result.goals_visit,
      goals_local: matchState.current?.result.goals_local,
      ret: matchState.current?.result.ret >=0 ? matchState.current?.result.ret+1: -1,  
    };

  const loginSchema = Yup.object().shape({
    goals_visit: Yup.number().required(),
    goals_local: Yup.number().required(),
    ret: Yup.number()
  });

  const theme = useTheme();

  return (<div className='edit'>
            <Dialog open={matchState.isFormVisible} onClose={handleClose} fullScreen>
              <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}><Typography>Editar Partido</Typography></BootstrapDialogTitle>
              <DialogContent>
                <Formik 
                  initialValues={initialValues} 
                  validationSchema={loginSchema} 
                  onSubmit={(values) => {handleSave(values);}}>
                  {
                    ({ handleBlur, errors, touched, dirty, values, handleChange, setFieldValue }) => (
                    <Form className='form' >
                      <div className='team'>
                        <div className='name'><Typography>{matchState.current?.team_local.name}</Typography></div>
                        <div className='set'>
                          <TextField 
                            id="goals_local" 
                            label="Goals" 
                            type="number"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.goals_local}
                            variant="standard"
                            helperText={touched.goals_local ? errors.goals_local : ""}
                            error={touched.goals_local && Boolean(errors.goals_local)}/>
                        </div>
                        
                      </div>
                      <div className='team'>
                        <div className='name'><Typography>{matchState.current?.team_visit.name}</Typography></div>
                        <div className='set'>
                          <TextField 
                            id="goals_visit" 
                            label="Goals" 
                            type="number"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            variant="standard"
                            value={values.goals_visit}
                            helperText={touched.goals_visit ? errors.goals_visit : ""}
                            error={touched.goals_visit && Boolean(errors.goals_visit)}/>
                        </div>
                      </div>
                      <div className='wo'>
                        <FormControl variant="filled" fullWidth >
                          <InputLabel id="demo-simple-select-label">Hay retiro? </InputLabel>
                          <Select
                            labelId="demo-simple-select-label"
                            name="ret"
                            value={values.ret > 0 ? values.ret : ''}
                            onChange={(value) => handleChangeRetWO(setFieldValue, value)}>
                              <MenuItem value={-1}></MenuItem>
                              {players.map((t, index) => 
                                <MenuItem value={index}>{t.name}</MenuItem>
                              )}
                          </Select>
                        </FormControl>
                      </div>
                      <div className='buttons'>
                        <div>
                          <Button variant="contained" type="submit">Guardar</Button>
                        </div>
                      </div>
                      
                    </Form>
                  )}
                </Formik>
                
              </DialogContent>
              <DialogActions>
                
              </DialogActions>
            </Dialog>
        </div>);
}

const mapStateToProps = (state: RootState) => ({
  matchState: getMatch(state)
});

export default connect(mapStateToProps)(MatchEditSoccer);