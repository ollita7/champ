import React, { ReactElement, useEffect } from 'react';
import { connect } from "react-redux";
import { useLocation } from "react-router-dom";
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

import { getApplication } from '../../store/selectors';
import { RootState } from "../../store/store";
import { IApplicationState, setNeedsUpdate, setApplication } from '../../store/reducers/application';
import { useGetTournaments } from '../../network/services/tournament.service';
import { IStoreDispatchProps } from '../../store/storeComponent';
import './styles.scss';

export interface IGroupsProps extends IStoreDispatchProps{
  application: IApplicationState;
}

const CategorySelect: React.FC<IGroupsProps> = ({application, ...props }): ReactElement => {
  const search = useLocation().search;
  const event_param = new URLSearchParams(search).get('event');
  let { data: tournaments, isLoading: torunamentsIsLoading, isSuccess: tournamentsIsSuccess } = useGetTournaments(application?.event);
 
  useEffect(() => {
    if(!application.tournament && !application.category && tournamentsIsSuccess && tournaments.length > 0){
      if (event_param){
        tournaments = tournaments.filter(t => t.id == event_param);
      }
      else {
        tournaments = tournaments.filter(t => t.current == true);
      }
      let current_stage = 'q';
      if (!tournaments[0].categories[0].show_group && tournaments[0].categories[0].stages.length > 0){
        current_stage = tournaments[0].categories[0].stages[0];
      }
      const newState: IApplicationState = {
        event: tournaments[0].id,
        category: tournaments[0].categories[0].category_id,
        tournament: tournaments[0].categories[0].tournament_id,
        cups: tournaments[0].categories[0].cups,
        stages: tournaments[0].categories[0].stages,
        sport_type: tournaments[0].categories[0].sport_type,
        current_stage: current_stage,
        needsUpdate: true,
        show_group: tournaments[0].categories[0].show_group,
        members: tournaments[0].categories[0].members
      };
      props.dispatch(setApplication(newState))
    }
  }, [tournaments]);

  const handleChange = (event) => {
    let current_stage = 'q';
    if (!event.target.value.show_group && event.target.value.stages.length > 0){
      current_stage = event.target.value.stages[0];
    }
    const newState: IApplicationState = {
      event: application.event,
      category: event.target.value.category_id,
      current_stage: current_stage,
      tournament: event.target.value.tournament_id,
      cups: event.target.value.cups,
      stages: event.target.value.stages,
      show_group: event.target.value.show_group,
      needsUpdate: false,
      members: event.target.value.members
    };
    props.dispatch(setApplication(newState))
  }

  return (
    <>
      {(tournamentsIsSuccess && tournaments.length > 1) &&
        <div className='select-category'>
          <FormControl variant="filled" fullWidth >
            <InputLabel id="demo-simple-select-label">Categoría</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={tournaments.find(t => t.category_id == application.category && t.tournament_id == application.tournament) || tournaments[0]}
              label="Categoría"
              onChange={handleChange}
            >
              {tournaments.map(t => 
                <MenuItem key={t.category} value={t}>{t.category}</MenuItem>
              )}
            </Select>
          </FormControl>
        </div>
      }
    </>
    
  )
}

const mapStateToProps = (state: RootState) => ({
  application: getApplication(state),
});

export default connect(mapStateToProps)(CategorySelect);