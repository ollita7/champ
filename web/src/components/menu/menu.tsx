import React, { ReactElement, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { connect } from "react-redux";
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination, Autoplay } from "swiper/modules";

import AccountCircle from "@mui/icons-material/AccountCircle";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import MenuItem from "@mui/material/MenuItem";
import Button from "@mui/material/Button";
import { ROUTES } from "../../navigation/constants";
import { useLogout } from "../../network/services/user.service";
import { IStoreDispatchProps } from "../../store/storeComponent";
import { clearProfile } from "../../store/reducers/profile";
import { getApplication, getProfile } from "../../store/selectors";
import { RootState } from "../../store/store";
import { IProfileState } from "../../store/reducers/profile";
import LoginIcon from "@mui/icons-material/Login";

import "./styles.scss";
import "swiper/css";
import "swiper/css/pagination";
import { IApplicationState } from "../../store/reducers/application";
import { getTenant, useGetTenant } from "../../network/services/tenant.service";
import { Config } from "../../utils";

const pages = [
  { title: "Etapas", link: ROUTES.EVENTS },
  //{title: 'Grupos', link: ROUTES.GROUP},
  { title: "Cruces", link: ROUTES.HOME },
  //{title: 'Sponsors', link: ROUTES.SPONSORS}
];

export interface IResponsiveMenuProps extends IStoreDispatchProps {
  profile: IProfileState;
  application: IApplicationState;
}

const ResponsiveMenu: React.FC<IResponsiveMenuProps> = ({
  profile,
  application,
  ...props
}): ReactElement => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(
    null
  );
  const [sponsors, setSponsors] = useState<string[]>([]);
  const navigate = useNavigate();
  const tenant = getTenant();
  const { data, isLoading, isSuccess } = useGetTenant();

  useEffect(() => {
    const fetchSponsorImages = async (tenant: string) => {
      try {
        const response = await fetch(
          `${Config.API_URL}tenant/${tenant}/sponsors`
        );
        const data = await response.json();
        setSponsors(data.data || []);
      } catch (error) {
        setSponsors([]);
      }
    };

    if (tenant) {
      fetchSponsorImages(tenant);
    }
  }, [tenant]);

  const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = (page?: string) => {
    setAnchorElNav(null);
    if (page) navigate(page);
  };

  const handleTermsHtml = (termsHtml: string) => {
    setAnchorElNav(null);
    navigate(ROUTES.TERMS, { state: { termsHtml } });
  };

  const handleLogout = () => {
    useLogout();
    props.dispatch(clearProfile());
    handleClose();
  };

  const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className="menu">
      <AppBar position="fixed" className="header">
        <Container maxWidth="xl">
          <Toolbar disableGutters className="toolbar">
            <div className="main-logo">
              <img src={data?.logo} alt="logo" />
            </div>
            {/*
            <div className='atpe'>
              <img src={Logo} alt="logo" />
            </div>
            */}
            <Box
              sx={{
                flexGrow: 1,
                display: {
                  xs: "flex",
                  md: "none",
                  justifyContent: "flex-end",
                  alignItems: "baseline",
                },
              }}
            >
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleOpenNavMenu}
                color="inherit"
              >
                <MenuIcon />
              </IconButton>

              <Menu
                id="menu-appbar"
                anchorEl={anchorElNav}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "left",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "left",
                }}
                open={Boolean(anchorElNav)}
                onClose={() => handleCloseNavMenu()}
                sx={{
                  display: { xs: "block", md: "none" },
                }}
              >
                {pages.map((page) => (
                  <MenuItem
                    key={page.title}
                    onClick={() => handleCloseNavMenu(page.link)}
                  >
                    <Typography textAlign="center">{page.title}</Typography>
                  </MenuItem>
                ))}
                {isSuccess && data && data.ranking && (
                  <MenuItem
                    key="ranking"
                    onClick={() => handleCloseNavMenu(ROUTES.RANKING)}
                  >
                    <Typography textAlign="center">Ranking</Typography>
                  </MenuItem>
                )}
                {application.sport_type == "soccer" && (
                  <MenuItem
                    key="Estadisticas"
                    onClick={() => handleCloseNavMenu(ROUTES.STATISTICS)}
                  >
                    <Typography textAlign="center">Estadisticas</Typography>
                  </MenuItem>
                )}
                {isSuccess && data && data.terms && (
                  <MenuItem key="terms" onClick={() => handleTermsHtml(data.termsHtml)}>
                    <Typography textAlign="center">Reglamento</Typography>
                  </MenuItem>
                )}
              </Menu>
              {profile?._id ? (
                <MenuItem>
                  <div onClick={handleMenu}>
                    <AccountCircle />
                  </div>
                  <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                      vertical: "top",
                      horizontal: "right",
                    }}
                    keepMounted
                    transformOrigin={{
                      vertical: "top",
                      horizontal: "right",
                    }}
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                  >
                    <MenuItem>{profile.name}</MenuItem>
                    <MenuItem onClick={handleLogout}>
                      logout {"  "}
                      <LoginIcon />
                    </MenuItem>
                  </Menu>
                </MenuItem>
              ) : (
                <MenuItem onClick={() => navigate(ROUTES.LOGIN)}>
                  <LoginIcon />
                </MenuItem>
              )}
            </Box>
            {/*<Button color="inherit" onClick={() => navigate(ROUTES.LOGIN)}>Login</Button>*/}
            {}
            <Box
              sx={{
                flexGrow: 1,
                display: { xs: "none", md: "flex", justifyContent: "flex-end" },
                maxHeight: "56px",
                gap: "1rem",
              }}
            >
              {pages.map((page) => (
                <Button
                  key={page.title}
                  onClick={() => handleCloseNavMenu(page.link)}
                  sx={{ my: 2, color: "white", display: "block", padding: 0 }}
                >
                  {page.title}
                </Button>
              ))}
              {isSuccess && data && data.ranking && (
                <Button
                  key="terms"
                  onClick={() => handleCloseNavMenu(ROUTES.RANKING)}
                  sx={{ my: 2, color: "white", display: "block", padding: 0 }}
                >
                  Ranking
                </Button>
              )}
              {application.sport_type == "soccer" && (
                <Button
                  key="statistic"
                  onClick={() => handleCloseNavMenu(ROUTES.STATISTICS)}
                  sx={{ my: 2, color: "white", display: "block", padding: 0 }}
                >
                  <span style={{ paddingRight: 5 }}>Estadisticas</span>
                </Button>
              )}
              {isSuccess && data && data.terms && (
                <Button
                  key="terms"
                  onClick={() => handleTermsHtml(data.termsHtml)}
                  sx={{ my: 2, color: "white", display: "block", padding: 0 }}
                >
                  Reglamento
                </Button>
              )}
              {profile?._id ? (
                <Button
                  sx={{ my: 2, color: "white", display: "block", padding: 0 }}
                >
                  <span onClick={handleMenu}>
                    <AccountCircle />
                  </span>
                  <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                      vertical: "top",
                      horizontal: "right",
                    }}
                    keepMounted
                    transformOrigin={{
                      vertical: "top",
                      horizontal: "right",
                    }}
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                  >
                    <MenuItem>{profile.name}</MenuItem>
                    <MenuItem onClick={handleLogout}>
                      logout {"  "}
                      <LoginIcon />
                    </MenuItem>
                  </Menu>
                </Button>
              ) : (
                <Button
                  onClick={() => navigate(ROUTES.LOGIN)}
                  sx={{ my: 2, color: "white", display: "block", padding: 0 }}
                >
                  <LoginIcon />
                </Button>
              )}
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
      <div className="sponsors">
        {sponsors.length > 0 && (
          <Swiper
            modules={[Pagination, Autoplay]}
            spaceBetween={20}
            slidesPerView={2}
            loop={true}
            autoplay={{ delay: 3000, disableOnInteraction: false }}
            pagination={{ clickable: true }}
          >
            {sponsors.map((sponsor, index) => (
              <SwiperSlide key={index}>
                <img
                  src={sponsor}
                  alt={`Sponsor ${index + 1}`}
                  className="sponsor-image"
                />
              </SwiperSlide>
            ))}
          </Swiper>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  profile: getProfile(state),
  application: getApplication(state),
});

export default connect(mapStateToProps)(ResponsiveMenu);
