import React, { ReactElement, useEffect } from 'react';
import { connect } from "react-redux";
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import { Typography } from '@mui/material';
import Collapse from '@mui/material/Collapse';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';

import { getApplication } from '../../store/selectors';
import { RootState } from "../../store/store";
import { IApplicationState} from '../../store/reducers/application';
import { IStoreDispatchProps } from '../../store/storeComponent';
import { IEvent } from '../../network/services/tournament.service';
import { ExpandMore } from '../../components/shared/expand-more';
import LinkIcon from '@mui/icons-material/Link';

import './styles.scss'

export interface ITournamentProps extends IStoreDispatchProps{
  application: IApplicationState;
  event: IEvent;
  onClick: any;
}

const Tournament: React.FC<ITournamentProps> = ({event, application, onClick, ...props }): ReactElement => {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
      <Card className={event.current ? 'tournament current': 'tournament'} sx={{ minWidth: 275 }}>
        <CardContent  onClick={() => onClick(event.categories[0].tournament_id, event.categories[0].category_id, event.id, event.categories[0].stages, event.categories[0].cups)}>
          <Typography sx={{ fontSize: 16 }} color="var(--text)" gutterBottom>
            {event.name}
          </Typography>
          <Typography sx={{ fontSize: 14 }} color="var(--text)" gutterBottom>
            {event.description} 
          </Typography>
        </CardContent>
        {event.categories.length > 1 &&
          <>
            <CardActions className='card-actions'>
              <LinkIcon onClick={() => {navigator.clipboard.writeText(`${window.location.host}?event=${event.id}`)}}></LinkIcon>
              <ExpandMore
                expand={expanded}
                onClick={handleExpandClick}
                aria-expanded={expanded}
                aria-label="show more"
                name='Ver Categorias'
                title='dasd'
                value='fsdfsad'
                className='expand-more'
              >
                <span className='ver' >Ver Categorias</span>
                <ExpandMoreIcon className='more-icon'/>
              </ExpandMore>
            </CardActions>
            <Collapse className='collapse' in={expanded} timeout="auto" unmountOnExit>
              <List className='category-list'>
                {event.categories.map(c =>
                  <ListItem className='category' onClick={() => onClick(c.tournament_id, c.category_id, event.id, c.stages, c.cups)}>
                    <ListItemText primary={c.category} secondary={new Date(c.date).toLocaleDateString('es-es', { weekday:"long", month:"long", day:"numeric"})} />
                  </ListItem>
                )}
              </List>
            </Collapse>
          </>
        }
      </Card>
  )}
     

const mapStateToProps = (state: RootState) => ({
  application: getApplication(state),
});

export default connect(mapStateToProps)(Tournament);