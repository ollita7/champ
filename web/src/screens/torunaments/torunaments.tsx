import React, { ReactElement, useEffect } from 'react';
import { connect } from "react-redux";
import { useNavigate } from 'react-router-dom';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CircularProgress from '@mui/material/CircularProgress';
import { Typography } from '@mui/material';
import Collapse from '@mui/material/Collapse';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';

import { getApplication } from '../../store/selectors';
import { RootState } from "../../store/store";
import { IApplicationState, setApplication} from '../../store/reducers/application';
import { IStoreDispatchProps } from '../../store/storeComponent';
import { useGetTournaments } from '../../network/services/tournament.service';
import { ROUTES } from '../../navigation/constants';
import { ExpandMore } from '../../components/shared/expand-more';

import './styles.scss'
import Tournament from './tournament';

export interface ITournamentsProps extends IStoreDispatchProps{
  application: IApplicationState;
}

const Tournaments: React.FC<ITournamentsProps> = ({application, ...props }): ReactElement => {
  const [expanded, setExpanded] = React.useState(false);
  const { data, isLoading, isSuccess, refetch } = useGetTournaments();
  const navigate = useNavigate();

  const handleClick = (tournament_id: string, category_id: string, event_id: string, stages, cups) => {
    const newState: IApplicationState = {
      category: category_id,
      event: event_id,
      tournament: tournament_id,
      stages: stages,
      cups: cups,
      needsUpdate: false
    };
    props.dispatch(setApplication(newState))
    navigate(ROUTES.HOME);
  }

  useEffect(() => {
    if(isSuccess && data.length == 1){
      //handleClick(data[0].categories[0].tournament_id, data[0].categories[0].category_id, data[0].event_id);
    }
  }, [isSuccess]);

  return (
    <div className='tournaments'>
      {isLoading && 
        <div className='loading'>
          <CircularProgress/>
          <Typography>Cargando</Typography>
        </div>
      }
      {isSuccess && data.map(c => 
        <Tournament event={c} onClick={handleClick}></Tournament>
      )}
     
    </div>
  )
}

const mapStateToProps = (state: RootState) => ({
  application: getApplication(state),
});

export default connect(mapStateToProps)(Tournaments);