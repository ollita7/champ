import React, { ReactElement, useEffect, useState } from 'react';
import { connect } from "react-redux";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { Form, Formik, useFormikContext } from "formik";
import * as Yup from "yup";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import { getMatch } from '../../../../store/selectors';
import { RootState } from "../../../../store/store";
import { IMatchState, setIsFormVisible } from '../../../../store/reducers/match';
import { setNeedsUpdate } from '../../../../store/reducers/application';
import { IStoreDispatchProps } from '../../../../store/storeComponent';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';

import './styles.scss'
import { Typography } from '@mui/material';
import { IAddResult, useAddResult } from '../../../../network/services/match.services';

export interface DialogTitleProps {
  id: string;
  children?: React.ReactNode;
  onClose: () => void;
}

function BootstrapDialogTitle(props: DialogTitleProps) {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other} className='header'>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: 'var(--details)'
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

export interface ITorunamentEditProps extends IStoreDispatchProps {
  //matchState: IMatchState
}

const TorunamentEdit: React.FC<ITorunamentEditProps> = ({ ...props }): ReactElement => {
  //const mutation = useAddResult();

  const handleClose = () => {
    //props.dispatch(setIsFormVisible(false));
  };

  const handleSave = (values) => {
    /*mutation.mutate(data, {
      onSuccess: async (response) => {
        props.dispatch(setNeedsUpdate(true));
        handleClose();
      },
    });*/
  }

  let initialValues = {
    set_1_visit: 0,
    set_1_local: 0,
    set_1_tie: false,
    set_1_local_tie: 0,  
    set_1_visit_tie: 0,
    set_2_visit: 0,
    set_2_local: 0,
    set_2_tie: false,
    set_2_local_tie: 0,  
    set_2_visit_tie: 0,
    super_local: 0,
    super_visit: 0,  
  };


  const loginSchema = Yup.object().shape({
    event_name: Yup.number().required(`El nombre es requerido`),
    event_description: Yup.number().required(`La descripción es reqeurida`),
    
  });

  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

  return (<div className='edit'>
            <Dialog open={false} onClose={handleClose} fullScreen={fullScreen}>
              <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}><Typography>Editar Partido</Typography></BootstrapDialogTitle>
              <DialogContent>
                <Formik 
                  initialValues={initialValues} 
                  validationSchema={loginSchema} 
                  onSubmit={(values) => {handleSave(values);}}>
                  {
                    ({ handleBlur, errors, touched, dirty, values, handleChange }) => (
                    <Form className='form' >
                      <div className='team'>
                        <span className='name'><Typography>{matchState.current?.team_local.name}</Typography></span>
                        <div className='set'>
                          <TextField 
                            id="set_1_local" 
                            label="Set 1" 
                            type="number"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.set_1_local}
                            variant="standard"
                            helperText={touched.set_1_local ? errors.set_1_local : ""}
                            error={touched.set_1_local && Boolean(errors.set_1_local)}/>
                        </div>
                        {isVisibleTie1(values) && <div className='set tie'>
                          <TextField 
                            id="set_1_local_tie" 
                            label="Tie" 
                            type="number"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.set_1_local_tie}
                            variant="standard"
                            helperText={touched.set_1_local_tie ? errors.set_1_local_tie : ""}
                            error={touched.set_1_local_tie && Boolean(errors.set_1_local_tie)}/>
                        </div>}
                        <div className='set'>
                          <TextField 
                            id="set_2_local" 
                            label="Set 2" 
                            type="number"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.set_2_local}
                            variant="standard"
                            helperText={touched.set_2_visit ? errors.set_2_visit : ""}
                            error={touched.set_2_visit && Boolean(errors.set_2_visit)}/>
                        </div>
                        {isVisibleTie2(values) &&<div className='set tie'>
                          <TextField 
                            id="set_2_local_tie" 
                            label="Tie" 
                            type="number"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.set_2_local_tie}
                            variant="standard"
                            InputProps={{ inputProps: { min: 0} }}
                            helperText={touched.set_2_local_tie ? errors.set_2_local_tie : ""}
                            error={touched.set_2_local_tie && Boolean(errors.set_2_local_tie)}/>
                        </div>}
                        {isVisibleSuper(values) && <div className='set'>
                          <TextField 
                            id="super_local" 
                            label="Super" 
                            type="number"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.super_local}
                            variant="standard"
                            helperText={touched.super_local ? errors.super_local : ""}
                            error={touched.super_local && Boolean(errors.super_local)}/>
                        </div>}
                      </div>
                      <div className='buttons'>
                        <div>
                          <Button variant="contained" type="submit">Guardar</Button>
                        </div>
                      </div>
                    </Form>
                  )}
                </Formik>
              </DialogContent>
            </Dialog>
        </div>);
}

const mapStateToProps = (state: RootState) => ({
});

export default connect(mapStateToProps)(TorunamentEdit);