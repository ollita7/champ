import React, { ReactElement, useEffect } from 'react';
import { connect } from "react-redux";
import './styles.scss'
import { getApplication } from '../../store/selectors';
import { RootState } from "../../store/store";
import { IApplicationState, setNeedsUpdate } from '../../store/reducers/application';
import { userGetTournament } from '../../network/services/tournament.service';
import { IStoreDispatchProps } from '../../store/storeComponent';
import CircularProgress from '@mui/material/CircularProgress';
import Group from './group';
import { Typography } from '@mui/material';
import CategorySelect from '../../components/category-select/category-select';

export interface IGroupsProps extends IStoreDispatchProps{
  application: IApplicationState;
}

const Groups: React.FC<IGroupsProps> = ({application, ...props }): ReactElement => {
  const { data: tournament, isLoading, isSuccess, refetch } = userGetTournament(application.tournament, application.category);
  
  useEffect(() => {
    if (application.needsUpdate) {
      refetch();
      props.dispatch(setNeedsUpdate(false))
    }
  }, [application.needsUpdate]);

  return (
    <div className={`groups-section ${tournament?.groups?.length === 1 ? 'single-group' : ''}`}>
      {/*<CategorySelect />*/}
      {isLoading && 
        <div className='loading'>
          <CircularProgress/>
          <Typography>Cargando</Typography>
        </div>}
      {isSuccess && tournament.groups?.map(group => 
        <Group group={group} key={group.name}></Group>
      )}
    </div>
  )
}

const mapStateToProps = (state: RootState) => ({
  application: getApplication(state),
});

export default connect(mapStateToProps)(Groups);