import React, { ReactElement, useState } from "react";
import { connect } from "react-redux";

import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead, { tableHeadClasses } from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { Matches } from "../../components/matches";
import Typography from "@mui/material/Typography";
import { styled } from "@mui/material/styles";
import Collapse from "@mui/material/Collapse";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { CircleFlag } from "react-circle-flags";
import {
  getStandings,
  getStandingsSoccer,
  IAutogenerate,
  useAutoGenerateGroupMatches,
} from "../../network/services/match.services";
import { ExpandMore } from "../../components/shared/expand-more";
import { getApplication, getProfile } from "../../store/selectors";
import { RootState } from "../../store/store";
import { IProfileState } from "../../store/reducers/profile";

import "./styles.scss";
import { IGroup } from "../../network/services/tournament.service";
import {
  setNeedsUpdate,
  IApplicationState,
} from "../../store/reducers/application";
import { IStoreDispatchProps } from "../../store/storeComponent";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  position: "relative",
  padding: "16px 5px",
  textAlign: "center",
  borderColor: "black",
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  position: "relative",
  "&:nth-of-type(odd)": {
    //backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

const StyledTableContainer = styled(TableContainer)(({ theme }) => ({
  boxShadow: "none",
}));

export interface IGroupsProps extends IStoreDispatchProps {
  group: IGroup;
  profile: IProfileState;
  application: IApplicationState;
}

const Group: React.FC<IGroupsProps> = ({
  application,
  profile,
  group,
  ...props
}): ReactElement => {
  const [expanded, setExpanded] = React.useState(false);
  const mutation = useAutoGenerateGroupMatches();
  const standing =
    application.sport_type == "tennis"
      ? getStandings(group)
      : getStandingsSoccer(group);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const handleAutogenerate = () => {
    const data: IAutogenerate = {
      tournament_id: application.tournament || "",
      group: group.name,
    };
    mutation.mutate(data, {
      onSuccess: async (response) => {
        props.dispatch(setNeedsUpdate(true));
      },
    });
  };

  const drawHeader = () => {
    return (
      <>
        {application.sport_type == "tennis" && (
          <TableHead>
            <TableRow>
              <StyledTableCell></StyledTableCell>
              <StyledTableCell>PJ</StyledTableCell>
              <StyledTableCell>SF</StyledTableCell>
              <StyledTableCell>GF</StyledTableCell>
              <StyledTableCell>Pts</StyledTableCell>
            </TableRow>
          </TableHead>
        )}
        {application.sport_type == "soccer" && (
          <TableHead>
            <TableRow>
              <StyledTableCell></StyledTableCell>
              <StyledTableCell>PJ</StyledTableCell>
              <StyledTableCell>PP</StyledTableCell>
              <StyledTableCell>PE</StyledTableCell>
              <StyledTableCell>PG</StyledTableCell>
              <StyledTableCell>+/-</StyledTableCell>
              <StyledTableCell>Pts</StyledTableCell>
            </TableRow>
          </TableHead>
        )}
      </>
    );
  };

  return (
    <div className="group" key={group.name}>
      <Card sx={{ minWidth: 275 }}>
        <h2 className="name">
          <Typography color="var(--title)">{group.name}</Typography>
        </h2>
        <CardContent>
          {standing.length > 2 ? (
            <StyledTableContainer>
              <Table aria-label="simple table" className="table">
                {drawHeader()}
                <TableBody>
                  {standing.map((s, index) => (
                    <StyledTableRow
                      key={s.member.name}
                      className="table-row"
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    >
                      <StyledTableCell component="th" scope="row">
                        <div className="country">
                          {s.member.country ? (
                            <CircleFlag
                              className="flag"
                              countryCode={s.member.country?.toLowerCase()}
                              height="25"
                            />
                          ) : (
                            <CircleFlag
                              className="flag"
                              countryCode="uy"
                              height="25"
                            />
                          )}
                          <span className="member">
                            {s.member.name.toUpperCase()}{" "}
                          </span>
                        </div>
                      </StyledTableCell>
                      {application.sport_type == "tennis" && (
                        <>
                          <StyledTableCell component="th" scope="row">
                            {s.pj}
                          </StyledTableCell>
                          <StyledTableCell component="th" scope="row">
                            {s.sf}
                          </StyledTableCell>
                          <StyledTableCell component="th" scope="row">
                            {s.gf}
                          </StyledTableCell>
                          <StyledTableCell component="th" scope="row">
                            <strong>{s.pts}</strong>
                          </StyledTableCell>
                        </>
                      )}
                      {application.sport_type == "soccer" && (
                        <>
                          <StyledTableCell component="th" scope="row">
                            {s.pj}
                          </StyledTableCell>
                          <StyledTableCell component="th" scope="row">
                            {s.pp}
                          </StyledTableCell>
                          <StyledTableCell component="th" scope="row">
                            {s.pe}
                          </StyledTableCell>
                          <StyledTableCell component="th" scope="row">
                            {s.pg}
                          </StyledTableCell>
                          <StyledTableCell component="th" scope="row">
                            {s.dif}
                          </StyledTableCell>
                          <StyledTableCell component="th" scope="row">
                            <strong>{s.pts}</strong>
                          </StyledTableCell>
                        </>
                      )}
                    </StyledTableRow>
                  ))}
                </TableBody>
              </Table>
            </StyledTableContainer>
          ) : (
            <Matches group={group} />
          )}
        </CardContent>
        <CardActions className="actions">
          {profile?._id && (
            <Button variant="contained" onClick={handleAutogenerate}>
              Generar partidos
            </Button>
          )}
          {standing.length > 2 && (
            <ExpandMore
              expand={expanded}
              onClick={handleExpandClick}
              aria-expanded={expanded}
              aria-label="show more"
              name="Ver Partidos"
              title="dasd"
              value="fsdfsad"
              className="expand-more"
            >
              <span className="ver">Ver Partidos</span>
              <ExpandMoreIcon className="more-icon" />
            </ExpandMore>
          )}
        </CardActions>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <Matches group={group} />
          {standing.length > 3 && (
            <div className="less">
              <ExpandMore
                expand={expanded}
                onClick={handleExpandClick}
                aria-expanded={expanded}
                aria-label="show more"
                name="Ver Partidos"
                title="dasd"
                value="fsdfsad"
                className="expand-more"
              >
                <span className="ver">Ver menos</span>
                <ExpandMoreIcon className="more-icon" />
              </ExpandMore>
            </div>
          )}
        </Collapse>
      </Card>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  profile: getProfile(state),
  application: getApplication(state),
});

export default connect(mapStateToProps)(Group);
