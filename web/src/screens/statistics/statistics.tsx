import React, { ReactElement, useEffect } from 'react';
import { connect } from "react-redux";
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import { getApplication } from '../../store/selectors';
import { RootState } from "../../store/store";
import { IApplicationState, setApplication, setNeedsUpdate } from '../../store/reducers/application';
import { IStoreDispatchProps } from '../../store/storeComponent';
import { Typography } from '@mui/material';
import { useGetMatchesByStage, useGetTournaments, userGetTournamentStatistics } from '../../network/services/tournament.service';
import CircularProgress from '@mui/material/CircularProgress';
import { Match } from '../../components/matches/match';
import ArrowBackIosNewSharpIcon from '@mui/icons-material/ArrowBackIosNewSharp';
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';
import CategorySelect from '../../components/category-select/category-select';

import './styles.scss'
import Groups from '../groups/groups';
import StatisticTable from './statistic-table';

export interface IStatisticsProps extends IStoreDispatchProps {
  application: IApplicationState;
}

const Statistics: React.FC<IStatisticsProps> = ({ application, ...props }): ReactElement => {
  const { data, isLoading, isSuccess, refetch } = userGetTournamentStatistics(application.tournament, application.category);

  useEffect(() => {
    if (application.needsUpdate && application.current_stage != 'q') {
      refetch();
      props.dispatch(setNeedsUpdate(false))
    }
  }, [application.needsUpdate]);
  
  return (
    <div className='statistics'>
      <CategorySelect />
      <div className='matches' >
        {isLoading && 
          <div className='loading'>
            <CircularProgress/>
            <Typography>Cargando</Typography>
          </div>
        }
        {isSuccess && data && <>
          <StatisticTable name="GOLEADORES" statistics={data?.strikers} />  
          <StatisticTable name="VALLA MENOS VENCIDA" statistics={data?.goals_conceded} />
        </>}
      </div>
    </div>
  );
}

const mapStateToProps = (state: RootState) => ({
  application: getApplication(state),
});

export default connect(mapStateToProps)(Statistics);
