import React, { ReactElement, useState } from 'react';
import { connect } from "react-redux";

import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead, {tableHeadClasses} from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { Matches } from '../../components/matches';
import Typography from '@mui/material/Typography';
import { styled } from '@mui/material/styles';
import Collapse from '@mui/material/Collapse';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { CircleFlag } from 'react-circle-flags'
import { getStandings, getStandingsSoccer, IAutogenerate, useAutoGenerateGroupMatches } from '../../network/services/match.services';
import { ExpandMore } from '../../components/shared/expand-more';
import { getApplication, getProfile } from '../../store/selectors';
import { RootState } from "../../store/store";
import { IProfileState } from '../../store/reducers/profile';

import './styles.scss'
import { IGroup } from '../../network/services/tournament.service';
import { setNeedsUpdate, IApplicationState } from '../../store/reducers/application';
import { IStoreDispatchProps } from '../../store/storeComponent';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  position: 'relative',
  padding: '16px 5px',
  textAlign: 'center',
  borderColor: 'black',
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  position: 'relative',
  '&:nth-of-type(odd)': {
    //backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  }
}));

const StyledTableContainer = styled(TableContainer)(({ theme }) => ({
  boxShadow: 'none'
}));

export interface IStatistic {
  team: string;
  player: string;
  goals: number
}

export interface IStatisticTablesProps extends IStoreDispatchProps {
  statistics: IStatistic[];
  name: string
  profile: IProfileState;
  application: IApplicationState;
}

const StatisticTable: React.FC<IStatisticTablesProps> = ({ application, profile, statistics, name, ...props }): ReactElement => {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const drawHeader = (statistics) => {
    return (
        <TableHead>
          <TableRow>
            <StyledTableCell>Equipo</StyledTableCell>
            {statistics && statistics.length > 0 && statistics[0].player && <StyledTableCell>Jugador</StyledTableCell>}
            <StyledTableCell>Goles</StyledTableCell>
          </TableRow>
        </TableHead>
)
  }

  return (
    <div className='statistic-table' key={name}>
      <Card sx={{ minWidth: 275 }}>
        <h2 className='name' >
          <Typography color="var(--title)">{name}</Typography>
          </h2>
        <CardContent>
            <StyledTableContainer>
              <Table aria-label="simple table" className='table'>
                {drawHeader(statistics)}
                <TableBody >
                  {statistics?.map((s, index) => (
                    <StyledTableRow
                      key={s.team}
                      className='table-row'
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <StyledTableCell component="th" scope="row">
                        <span className='member'>{s.team?.toUpperCase()} </span>
                      </StyledTableCell>
                      {s.player && <StyledTableCell component="th" scope="row">{s.player ? s.player : ""}</StyledTableCell>}
                      <StyledTableCell component="th" scope="row"><strong>{s.goals}</strong></StyledTableCell>
                    </StyledTableRow>
                  ))}
                </TableBody>
              </Table>
            </StyledTableContainer>
        </CardContent>
      </Card>
    </div>
  )
}

const mapStateToProps = (state: RootState) => ({
  profile: getProfile(state),
  application: getApplication(state)
});

export default connect(mapStateToProps)(StatisticTable);