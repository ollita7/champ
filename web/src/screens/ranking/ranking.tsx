import React, { ReactElement } from 'react';
import Card from '@mui/material/Card';
import { CircleFlag } from 'react-circle-flags'
import CardContent from '@mui/material/CardContent';
import { Typography } from '@mui/material';

import './styles.scss'

export interface IRankingProps {

}

const items = [
  {position: 1, name: 'D. Ponte', country: 'AR', points:  1560},
  {position: 2, name: 'J. Ramos', country: 'UY', points: 1360},
  {position: 3, name: 'S. Ferro', country: 'UY', points:  1060},
  {position: 4, name: 'F. Rocha', country: 'UY', points:  1000},
  {position: 5, name: 'E. Dalmás', country: 'UY', points:  990},
  {position: 6, name: 'L. Viola ', country: 'UY', points: 720},
  {position: 6, name: 'I. Veiga', country: 'UY', points:  720},
  {position: 7, name: 'M. Sinigaglia', country: 'AR', points:  460},
  {position: 8, name: 'J. Van Kerckhoven', country: 'ZA', points:  390},
  {position: 9, name: 'J. Echavarría', country: 'UY', points:  290},
  {position: 10, name: 'M. Marella', country: 'UY', points:  240},
  {position: 10, name: 'M. Coore', country: 'UY', points:  240},
  {position: 11, name: 'M. Buzik', country: 'UY', points:  210},
  {position: 12, name: 'M. Toscanini', country: 'UY', points:  190},
  {position: 13, name: 'C. Obregón', country: 'UY', points:  180},
  {position: 13, name: 'S. Clavijo', country: 'UY', points:  180},
  {position: 14, name: 'G. Gorg', country: 'AR', points:  170},
  {position: 15, name: 'A. Llambías', country: 'UY', points:  140},
  {position: 16, name: 'I. Moro', country: 'UY', points:  120},
  {position: 17, name: 'A. Chifflet', country: 'UY', points:  100},
  {position: 18, name: 'R. Larocca', country: 'AR', points:  60},
  {position: 19, name: 'F. Ferrero', country: 'UY', points:  20},
  {position: 20, name: 'S. González', country: 'UY', points:  40},
  {position: 21, name: 'G. Fernández', country: 'UY', points:  30},
  {position: 22, name: 'J. Vázquez', country: 'UY', points:  20},
  {position: 22, name: 'P. Berro', country: 'UY', points:  20},
  {position: 23, name: 'R. Ferreri', country: 'AR', points:  10},
  {position: 23, name: 'G. Menendez', country: 'UY', points:  10},
  {position: 23, name: 'G. Pazos', country: 'UY', points:  10},
]

const Ranking: React.FC<IRankingProps> = ({ ...props }): ReactElement => {
  return (
    <div className='ranking'>
      <Card sx={{ minWidth: 275 }}>
        <CardContent>
        <div className='row'>
          <Typography className='pos'>POS</Typography>
          <Typography className='name'>NOMBRE</Typography>
          <Typography>PUNTOS</Typography>
        </div>
        {items.map(item => 
          <div className='row'>
            <Typography className='pos'>{item.position}</Typography>
            <div className='name'> 
              <CircleFlag className="flag" countryCode={item.country.toLocaleLowerCase()} height="25"/>
              <Typography>{item.name}</Typography>
            </div>
            <Typography>{item.points}</Typography>
          </div>
        )}
        </CardContent>
      </Card>
     
    </div>
  )
}

export default Ranking;