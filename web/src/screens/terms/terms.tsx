import React, { ReactElement, useState } from 'react';
import ResponsiveMenu from '../../components/menu/menu';
import './styles.scss'
import { useLocation } from 'react-router-dom';

interface TermsState {
  termsHtml: string;
}

const Terms: React.FC = ({ ...props }): ReactElement => {
  const location = useLocation();
  const { termsHtml } = location.state as TermsState || { termsHtml: "" };
  
  return (
    // <div className='terms'>
    //   <ResponsiveMenu/>
    //   <div className='pdf'>
    //     <iframe src="http://ocean-tour.netlify.app/reglamento.pdf#toolbar=0" width="100%" height="500px"></iframe>
    //   </div>
    // </div>
    <div style={{ padding: "20px" }}>
      <div
        dangerouslySetInnerHTML={{ __html: termsHtml }}
        style={{ lineHeight: "1.6" }}
      ></div>
    </div>
  );
}

export default Terms;