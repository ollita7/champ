import React, { ReactElement, useEffect, useState } from "react";
import { connect } from "react-redux";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { getApplication } from "../../store/selectors";
import { RootState } from "../../store/store";
import {
  IApplicationState,
  setApplication,
  setNeedsUpdate,
} from "../../store/reducers/application";
import { IStoreDispatchProps } from "../../store/storeComponent";
import { Typography } from "@mui/material";
import { useGetMatchesByTournament } from "../../network/services/tournament.service";
import CircularProgress from "@mui/material/CircularProgress";
import { TournamentBracket } from "../../components/matches/bracketView/tournamentBracket";
import ArrowBackIosNewSharpIcon from "@mui/icons-material/ArrowBackIosNewSharp";
import ArrowForwardIosSharpIcon from "@mui/icons-material/ArrowForwardIosSharp";
import CategorySelect from "../../components/category-select/category-select";
import { useMediaQuery } from "@mui/material";

import "./styles.scss";
import Groups from "../groups/groups";

export interface IQualifyingProps extends IStoreDispatchProps {
  application: IApplicationState;
}

const Qualifying: React.FC<IQualifyingProps> = ({
  application,
  ...props
}): ReactElement => {
  const isMobile = useMediaQuery("(max-width: 790px)");
  const [cup, setCup] = React.useState<string>("");
  const [selectedStage, setSelectedStage] = useState<string>(
    application.current_stage || ""
  );
  const {
    data: matches,
    isLoading,
    isSuccess,
    refetch,
  } = useGetMatchesByTournament(application.tournament, cup);

  const [view, setView] = React.useState<string>("groups");

  useEffect(() => {
    if (application.cups && application.cups.length > 0) {
      setCup(application.cups[0].id);
    } else {
      setCup("");
    }
  }, [application.cups]);

  const handleChange = (event: SelectChangeEvent) => {
    setCup(event.target.value as string);
  };

  useEffect(() => {
    if (application.needsUpdate && view === "brackets") {
      refetch();
      props.dispatch(setNeedsUpdate(false));
    }
  }, [application.needsUpdate, view]);

  useEffect(() => {
    if (!application.stages || application.stages.length === 0) {
      setSelectedStage("");
    } else {
      setSelectedStage(application.stages[0]);
    }
  }, [application.tournament, application.stages]);

  const stageIndex = application.stages?.indexOf(selectedStage) || 0;

  return (
    <div className="qualifying">
      <CategorySelect />
      {application?.cups && application?.cups?.length > 1 && (
        <FormControl variant="filled" fullWidth className="select-cup">
          <InputLabel id="demo-simple-select-label">Copa</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={cup}
            label="Copa"
            onChange={handleChange}
          >
            {application.cups.map((cup) => (
              <MenuItem value={cup.id}>{cup.name}</MenuItem>
            ))}
          </Select>
        </FormControl>
      )}
      <div className="title-menu">
        <div
          className={view === "groups" ? "arrow hidden" : "arrow"}
          onClick={() => {
            if (isMobile) {
              if (view === "brackets" && stageIndex > 0) {
                application.stages &&
                  setSelectedStage(application.stages[stageIndex - 1]);
              } else {
                setView("groups");
              }
            } else {
              setView("groups");
            }
          }}
        >
          <ArrowBackIosNewSharpIcon />
        </div>

        <div className="select-stage">
          {isMobile ? (
            <>
              {view === "groups" ? (
                <div
                  className="stage selected"
                  onClick={() => setView("groups")}
                >
                  <Typography>Grupos</Typography>
                </div>
              ) : (
                <div
                  className="stage selected"
                  onClick={() => setView("brackets")}
                >
                  <Typography>{selectedStage}</Typography>
                </div>
              )}
            </>
          ) : (
            <>
              <div
                className={view === "groups" ? "stage selected" : "stage"}
                onClick={() => setView("groups")}
              >
                <Typography>Grupos</Typography>
              </div>
              <div
                className={`stage ${view === "brackets" ? "selected" : ""} ${
                  application.stages && application.stages.length === 0
                    ? "disabled"
                    : ""
                }`}
                onClick={() => setView("brackets")}
              >
                <Typography>Partidos</Typography>
              </div>
            </>
          )}
        </div>
        <div
          className={
            (isMobile &&
              application.stages &&
              stageIndex === application.stages.length - 1) ||
            (view === "groups" &&
              application.stages &&
              application.stages.length === 0)
              ? "arrow hidden"
              : !isMobile && view === "brackets"
              ? "arrow hidden"
              : "arrow"
          }
          onClick={() => {
            if (
              view === "groups" &&
              application.stages &&
              application.stages.length > 0
            ) {
              setSelectedStage(application.stages[0]);
              setView("brackets");
            } else if (stageIndex < (application.stages?.length || 0) - 1) {
              application.stages &&
                setSelectedStage(application.stages[stageIndex + 1]);
            }
          }}
        >
          <ArrowForwardIosSharpIcon />
        </div>
      </div>

      <div className="matches">
        {isLoading && (
          <div className="loading">
            <CircularProgress />
            <Typography>Cargando</Typography>
          </div>
        )}
        <div className="tournament">
          {view === "brackets" && isSuccess && (
            <TournamentBracket
              matches={matches}
              selectedStage={selectedStage}
              setSelectedStage={setSelectedStage}
            />
          )}
        </div>
        <div className="groups">
          {view === "groups" && application.show_group && <Groups />}
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  application: getApplication(state),
});

export default connect(mapStateToProps)(Qualifying);
