import { ReactElement, useState } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { QueryClient, QueryClientProvider } from 'react-query'
import { Toaster } from 'react-hot-toast';
import { ThemeProvider, createTheme } from "@mui/material/styles";

import store from './store/store';
import { Navigator} from './navigation';
import { Typography } from '@mui/material';
import "../static/variables.scss";

 // Create a client
 const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: false,
      refetchOnWindowFocus: false
    },
  },
})

const theme = createTheme({
  palette: {
    primary: {
      main: "#544984"
    },
    secondary: {
      main: "#837ba6"
    }
  },
  typography: {
    allVariants: {
      fontSize: 14
    }
  }
});

const App = (): ReactElement => {
  

  return (
    <ThemeProvider theme={theme}>
      <QueryClientProvider client={queryClient}>
        <Provider store={store}>
          <BrowserRouter>
            <Navigator />
          </BrowserRouter>
        </Provider>
        <Typography>
          <Toaster position="bottom-center"/>
        </Typography>
      </QueryClientProvider>
    </ThemeProvider>
  );
};

export { App };