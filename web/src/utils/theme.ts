export const setTheme = (root, data) => {
  root?.style.setProperty("--background", data.theme.background);
  root?.style.setProperty("--card", data.theme.card);
  root?.style.setProperty("--matches-background", data.theme.matchesBackground);
  root?.style.setProperty("--details", data.theme.details);
  root?.style.setProperty("--title", data.theme.title);
  root?.style.setProperty("--menu", data.theme.menu);
  root?.style.setProperty("--one", data.theme.one);
  root?.style.setProperty("--two", data.theme.two);
  root?.style.setProperty("--text", data.theme.text);
  root?.style.setProperty("--group-texts", data.theme.groupTexts);
  
}