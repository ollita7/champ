import {
  createKiwiServer,
  IKiwiOptions,
  AuthorizeResponse,
  ON_EXCEPTION,
  getKiwiEmitter,
} from "kiwi-server";
import { UserController } from "./controllers/user.controller";
import { UserService } from "./services/user.services";
import { HeadersMiddleware } from "./middlewares/headers";
import { LogService, LEVELS, ILog } from "./services/log.services";
import { TournamentController } from "./controllers/tournament.controller";
import { MatchController } from "./controllers/match.controller";
import { TenantController } from "./controllers/tenant.controller";

let jwt = require("jwt-simple");
require("dotenv").config();

async function validateAuthentication(
  request: any,
  roles: Array<string>
): Promise<AuthorizeResponse | boolean> {
  const token = request.headers["authorization"];
  if (!token) {
    return new AuthorizeResponse(401, "User is not authenticated");
  }
  try {
    const decoded = jwt.decode(
      token.replace("Bearer ", ""),
      process.env.JWT_SECRET
    );

    if (roles?.length && !roles.includes(decoded.role)) {
      return new AuthorizeResponse(
        403,
        "User is not authorized to perform this action"
      );
    }
    request["userProfile"] = {
      ...decoded,
      account: request.headers["account"],
      project: request.headers["project"],
    };

    return true;
  } catch (ex) {
    return new AuthorizeResponse(401, "Invalid token");
  }
}

const options: IKiwiOptions = {
  controllers: [
    UserController,
    TournamentController,
    MatchController,
    TenantController,
  ],
  authorization: validateAuthentication,
  middlewares: [HeadersMiddleware],
  cors: {
    enabled: true,
    domains: [
      "http://localhost:8080",
      "https://develop.clashes.app",
      "https://test.clashes.app",
      "https://develop.clashes.app",
      "https://ocean-tour.clashes.app",
      "https://teniseventos.clashes.app",
      "https://marak.clashes.app",
      "https://marcla.clashes.app",
      "https://divino.clashes.app",
      "https://ccc.clashes.app",
    ],
  },
  documentation: {
    enabled: true,
    path: "/doc",
    suffix: "/api",
  },
  prefix: "/v1",
  socket: {
    enabled: false,
  },
  log: false,
  port: parseInt(process.env.PORT),
};

createKiwiServer(options);
const log: LogService = new LogService();

const emitter = getKiwiEmitter();
emitter.on(ON_EXCEPTION, (err: any) => {
  const data: ILog = {
    message: err.message,
    type: "nodejs",
    info: {},
    exception: {
      stack: err.stack,
    },
  };
  log.log(data, LEVELS.error);
});
