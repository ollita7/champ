import {
  JsonController,
  Get,
  Authorize,
  HeaderParam,
  Post,
  Body,
  Put,
  Param,
} from "kiwi-server";
import { TenantService } from "../services/tenant.services";

import { ResponseCode } from "../sdk/constants";
import { Response } from "../sdk/response";
import { GenericModel, UserProfile } from "../models/generic.model";
import { TenantIn } from "../models/tenant.model";
import { DeepPartial } from "typeorm";

@JsonController("/tenant")
export class TenantController {
  constructor(private tenantService: TenantService) {}

  @Get("")
  public async getUser(@HeaderParam("tenant") tenant: string) {
    let result = await this.tenantService.get(tenant);
    return new Response(ResponseCode.OK, "", result);
  }

  @Authorize()
  @Get("/user-tenant")
  public async getUserTenant(request: GenericModel) {
    const userProfile: UserProfile = request.userProfile;
    const tenants = await this.tenantService.getByUserTenants(userProfile);

    if (tenants) {
      return new Response(ResponseCode.OK, "", tenants);
    } else {
      return new Response(ResponseCode.UNAUTHORIZED, "User is not authorized");
    }
  }

  @Authorize(["super_admin"])
  @Post("")
  public async createTenant(@Body() tenant: TenantIn) {
    const result = await this.tenantService.createTenant(tenant);
    return new Response(ResponseCode.OK, "", result);
  }

  @Authorize(["super_admin"])
  @Put("/:id")
  public async updateTenant(
    @Param("id") id: string,
    @Body() tenantData: DeepPartial<TenantIn>
  ) {
    const result = await this.tenantService.updateTenant(id, tenantData);
    return new Response(ResponseCode.OK, "Tenant actualizado", result);
  }

  @Get("/:tenantName/sponsors")
  public async getSponsorImages(@Param("tenantName") tenantName: string) {
    const images = await this.tenantService.getSponsorImages(tenantName);
    if (images.length > 0) {
      return new Response(ResponseCode.OK, "", images);
    } else {
      return new Response(ResponseCode.ERROR, "No images found");
    }
  }
}
