import {
  JsonController,
  Get,
  Put,
  Post,
  Authorize,
  Body,
  Param,
  HeaderParam,
} from "kiwi-server";
import { ResponseCode } from "../sdk/constants";
import { Response } from "../sdk/response";
import { MatchService } from "../services/match.service";
import {
  ResultIn,
  MatchUpdateIn,
  MatchIn,
  AutogenerateIn,
  AutogenerateQualifyingsIn,
} from "../models/match.model";

@Authorize()
@JsonController("/match")
export class MatchController {
  constructor(private matchService: MatchService) {}

  @Put("/:id/result")
  public async updateMatchResult(
    @Param("id") match_id: string,
    @Body() result: ResultIn,
    @HeaderParam("tenant") tenant: string
  ) {
    let response = await this.matchService.updateMatchResult(
      match_id,
      result,
      tenant
    );
    return new Response(ResponseCode.OK, "Resultado agregado", response);
  }

  @Put("/:id")
  public async updateMatch(
    @Param("id") match_id: string,
    @Body() data: MatchUpdateIn,
    @HeaderParam("tenant") tenant: string
  ) {
    let response = await this.matchService.updateMatchDate(
      match_id,
      data,
      tenant
    );
    return new Response(ResponseCode.OK, "Partido agendado", response);
  }

  @Post("")
  public async addMatchToTournament(
    @Body() match: MatchIn,
    @HeaderParam("tenant") tenant: string
  ) {
    let result = await this.matchService.addMatch(match, tenant);
    return new Response(ResponseCode.OK, "", result);
  }

  @Post("/autogenerate")
  public async autogenerate(
    @Body() data: AutogenerateIn,
    @HeaderParam("tenant") tenant: string
  ) {
    let result = await this.matchService.autogenerate(data, tenant);
    return result;
  }

  @Post("/autogenerate/qualifyings")
  public async autogenerateQualifyings(
    @Body() data: AutogenerateQualifyingsIn,
    @HeaderParam("tenant") tenant: string
  ) {
    let result = await this.matchService.autogenerateQualifyings(data, tenant);
    return new Response(ResponseCode.OK, "", result);
  }
}
