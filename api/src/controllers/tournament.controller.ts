import {
  JsonController,
  Get,
  Put,
  Post,
  Authorize,
  Body,
  Param,
  HeaderParam,
} from "kiwi-server";
import { ResponseCode } from "../sdk/constants";
import { Response } from "../sdk/response";
import { TournamentService } from "../services/tournament.service";

@JsonController("/tournament")
export class TournamentController {
  constructor(private tournamentService: TournamentService) {}

  @Get("/:tournament_id/category/:category_id/statistics")
  public async getStatistics(
    @Param("tournament_id") tournament_id: string,
    @Param("category_id") category: string,
    @HeaderParam("tenant") tenant: string
  ) {
    let result = await this.tournamentService.getStatistics(
      tournament_id,
      category,
      tenant
    );
    return new Response(ResponseCode.OK, "", result);
  }

  @Get("/match/:tournamentId/group/:group")
  public async getMatchesByTournament(
    @Param("tournamentId") tournamentId: string,
    @HeaderParam("tenant") tenant: string,
    @Param("group") group?: string,
  ) {
    const result = await this.tournamentService.getMatchesByTournament(
      tournamentId,
      tenant,
      group,
    );
    return new Response(ResponseCode.OK, "", result);
  }

  @Get("/:tournament_id/category/:category_id/stage/:stage")
  public async getTournamentByCategory(
    @Param("tournament_id") tournament_id: string,
    @Param("category_id") category_id: string,
    @Param("stage") stage: string,
    @HeaderParam("tenant") tenant: string
  ) {
    let result = await this.tournamentService.getMatchesByStage(
      tournament_id,
      category_id,
      stage,
      tenant
    );
    return new Response(ResponseCode.OK, "", result);
  }

  @Get("/:tournament_id/category/:category_id/stage/:stage/group/:group")
  public async getTournamentByCategoryAndGroup(
    @Param("tournament_id") tournament_id: string,
    @Param("category_id") category_id: string,
    @Param("stage") stage: string,
    @Param("group") group: string,
    @HeaderParam("tenant") tenant: string
  ) {
    let result = await this.tournamentService.getMatchesByStage(
      tournament_id,
      category_id,
      stage,
      group,
      tenant
    );
    return new Response(ResponseCode.OK, "", result);
  }

  @Get("/:tournament_id/category/:category_id")
  public async getMatches(
    @Param("tournament_id") tournament_id: string,
    @Param("category_id") category_id: string,
    @HeaderParam("tenant") tenant: string
  ) {
    let result = await this.tournamentService.getTournamentCategory(
      tournament_id,
      category_id,
      tenant
    );
    return new Response(ResponseCode.OK, "", result);
  }

  @Get("/:tournament_id/category/:category_id/members")
  public async getTournamentMembersByCategory(
    @Param("tournament_id") tournament_id: string,
    @Param("category_id") category_id: string,
    @HeaderParam("tenant") tenant: string
  ) {
    let result = await this.tournamentService.getTournamentMembersByCategory(
      tournament_id,
      category_id,
      tenant
    );
    return new Response(ResponseCode.OK, "", result);
  }

  @Get("")
  @Get("/:event_id")
  public async getTournamentsByTenantEvent(
    @Param("event_id") event_id: string,
    @HeaderParam("tenant") tenant: string
  ) {
    let result = await this.tournamentService.getTournamentsByTenant(
      tenant,
      event_id,
      tenant
    );
    return new Response(ResponseCode.OK, "", result);
  }
}
