import { IsArray, IsNumber, IsString } from "kiwi-server";

export class GenericModel {
  userProfile: UserProfile
}

export class UserProfile {
  @IsString() _id: string;
  @IsString() name: string;
  @IsString() email: string;
  @IsString() password: string;
  @IsString() role: string;
  @IsString() picture: string;
  @IsArray() tenant_id: string[];
}