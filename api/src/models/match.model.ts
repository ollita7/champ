import { Team } from "datastore/entities";
import { IsString, IsNumber } from "kiwi-server";

export class SetIn{
  @IsString() games: Array<number>;
  @IsString() tie: Array<number>;
}

export class ResultIn{
  @IsString() set_1: SetIn;
  @IsString() set_2: SetIn;
  @IsString() super: Array<number>;
  @IsString() winner: number;
}

export class TeamIn {
  @IsString() id: string;
  @IsString() name: string;
  @IsString() country: string;
}

export class MatchUpdateIn{
  @IsString() date: Date;
  @IsString() team_local: TeamIn;
  @IsString() team_visit: TeamIn;
}

export class MatchIn{  
  @IsString() category_id: string;
  @IsString() tournament_id: any;
  @IsString() date: Date | string;
  @IsString() group: string;
  @IsString() team_local: TeamIn;
  @IsString() team_visit: TeamIn;
  @IsString() result: ResultIn;
  @IsString() stage: string; 
}

export class AutogenerateIn{  
  @IsString() tournament_id: string;
  @IsString() group: string;
}

export class AutogenerateQualifyingsIn{  
  @IsString() tournament_id: string;
  @IsString() stage: string;
  @IsString() cup: string;
  @IsNumber() total: number;
}

export class Statistics{  
  team: string;
  goals: number;
  goals_conceded: number;
}