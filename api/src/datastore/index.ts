import 'reflect-metadata';
import { createConnection, Connection, ConnectionOptions } from "typeorm";
import { User, Tournament, Match, Tenant } from './entities';
import { MatchSoccer } from './entities/match.entity';

require('dotenv').config();

const connection_options: ConnectionOptions = {
    type: "mongodb",
    name: '',
    host: process.env.TYPEORM_HOST,
    port: +process.env.TYPEORM_PORT,
    username: process.env.TYPEORM_USERNAME,
    password: process.env.TYPEORM_PASSWORD,
    database: process.env.TYPEORM_DATABASE,
    logging: true,
    authSource: 'admin',
    useUnifiedTopology: true,
    entities: [
        User,
        Tournament,
        Match,
        MatchSoccer,
        Tenant
    ],
}
const connection: Promise<void | Connection> = createConnection(connection_options).catch((error: any) => console.log(error));


export async function getRepository<T>(arg: { new(): T; }, tenant: string = null): Promise<any> {
    const conn = await connection;
    if (!conn) throw new Error('Connection to db not available');

    const repository = await conn.getMongoRepository(arg);
    if(tenant){
        repository.metadata.tableName = `${repository.metadata.tableName.split('_')[0]}_${tenant}`;
    }else{
        repository.metadata.tableName = repository.metadata.tableName.split('_')[0]
    }
    return repository;
}