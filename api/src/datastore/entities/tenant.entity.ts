
import { ObjectId } from "mongodb";
import { Entity, ObjectIdColumn, Column } from "typeorm";;


export class Theme {
    background : string;
    card : string;
    matchesBackground: string;
    details : string;
    title : string;
    menu : string;
    one : string;
    two : string;
    text : string;
    groupTexts: string;
}

@Entity('tenant')
export class Tenant {
    @ObjectIdColumn()
    _id: ObjectId;

    @Column()
    name: string;

    @Column()
    status: string;

    @Column()
    terms: string;

    @Column()
    termsHtml: string;

    @Column()
    logo: string;

    @Column()
    ranking: boolean;

    @Column()
    sponsor: boolean;

    @Column()
    theme: Theme;

}