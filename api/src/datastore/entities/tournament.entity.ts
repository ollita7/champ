
import { ObjectId } from "mongodb";
import { Entity, ObjectIdColumn, Column } from "typeorm";
import { Group } from "./index"

export class Cup {
    id: string;
    name: string;
}

export class Event {
    id: string;
    name: string;
    date: Date;
    description: string;
    sport_type: string;
    categories: any[];
    current: boolean;
}

export class TournamentReduced {
    tournament_id: string;
    date: Date;
    category_id: string;
    category: string;
    sport_type: string;
    stages: Array<string>;
    cups?: Array<Cup>;
    members: Array<any>;
    show_group: boolean;
}

@Entity('tournament')
export class Tournament {
    @ObjectIdColumn()
    _id: ObjectId;

    @Column()
    tenant_id: string;

    @Column()
    event_id: string;

    @Column()
    event_name: string;

    @Column()
    sport_type: string;

    @Column()
    event_description: string;

    @Column()
    date: Date;

    @Column()
    category_id: string;

    @Column()
    category: string;

    @Column()
    groups: Array<Group>;

    @Column()
    stages: Array<string>;

    @Column()
    cups?: Array<Cup>;

    @Column()
    statistics?: Array<any>;

    @Column()
    location: string;

    @Column()
    current: boolean;

    @Column()
    show_group: boolean;
}