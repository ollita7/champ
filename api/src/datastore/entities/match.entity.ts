
import { ObjectId } from "mongodb";
import { Entity, ObjectIdColumn, Column } from "typeorm";
import { Team } from "./index"

export class Set{
    @Column()
    games: Array<number>;

    @Column()
    tie: Array<number>;
  }


export class Result{
    @Column()
    set_1: Set;

    @Column()
    set_2: Set;

    @Column()
    super: Array<number>;

    @Column()
    winner: number;

  }

  export class ResultSoccer{
    @Column()
    goals_local: number;

    @Column()
    goals_visit: number;

    @Column()
    ret: number;

  }
  


@Entity('match')
export class Match {
    @ObjectIdColumn()
    _id: ObjectId;
    
    @Column()
    category_id: string;

    @Column()
    tournament_id: ObjectId;

    @Column()
    date: Date | string;

    @Column()
    group: string;

    @Column()
    team_local: Team;

    @Column()
    team_visit: Team;

    @Column()
    result: Result | ResultSoccer; 

    @Column()
    stage: string; 

}

@Entity('match')
export class MatchSoccer {
    @ObjectIdColumn()
    _id: ObjectId;
    
    @Column()
    category_id: string;

    @Column()
    tournament_id: ObjectId;

    @Column()
    date: Date | string;

    @Column()
    group: string;

    @Column()
    team_local: Team;

    @Column()
    team_visit: Team;

    @Column()
    result: ResultSoccer; 

    @Column()
    stage: string; 

}
