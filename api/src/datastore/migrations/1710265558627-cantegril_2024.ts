import { MigrationInterface, QueryRunner } from "typeorm"
import { Tournament, Group, Match, Team } from "../entities";
import { MongoQueryRunner } from "typeorm/driver/mongodb/MongoQueryRunner";
import { ObjectId } from "mongodb";

export class Cantegril20241710265558627 implements MigrationInterface {

 
    


    public async up(queryRunner: MongoQueryRunner): Promise<void> {
        let stages: any = [{id: 'CC12024', description: '1 - 1 al 13 de abril', date: new Date(2024,3,1)},      
        {id: 'CC22024', description: '2 - 1 al 22 de junio', date: new Date(2024,5,1)},
        {id: 'CC32024', description: '3 - 20 de julio al 11 de agosto', date: new Date(2024,6,20)},
        {id: 'CC42024', description: '4 - 28 de setiembre al 17 de octubre', date: new Date(2024,8,28)},
        {id: 'CC52024', description: '5 - 23 de noviembre al 15 de diciembre', date: new Date(2024,10,23)}];

        let categories: any = [{code: 'MA', description: 'MASCULINO A'}, 
        {code: 'MB', description: 'MASCULINO B'},
        {code: 'MC', description: 'MASCULINO C'},
        {code: 'FA', description: 'FEMENINO A'}, 
        {code: 'FB', description: 'FEMENINO B'},
        {code: 'FC', description: 'FEMENINO C'},
        {code: 'DMA', description: 'DOBLES MASCULINO A'}, 
        {code: 'DMB', description: 'DOBLES MASCULINO B'},
        {code: 'DMC', description: 'DOBLES MASCULINO C'},
        {code: 'DFA', description: 'DOBLES FEMENINO A'}, 
        {code: 'DFB', description: 'DOBLES FEMENINO B'},
        {code: 'DFC', description: 'DOBLES FEMENINO C'},
        {code: 'MXA', description: 'MIXTO A'}, 
        {code: 'MXB', description: 'MIXTO B'},
        {code: 'MXC', description: 'MIXTO C'}];
    
        let tournaments: any = [];
        stages.forEach(function(stage: any){
            categories.forEach(function(value: any){
                let tournament = new Tournament();        
                tournament._id = new ObjectId();
                tournament.date = stage['date'];
                tournament.event_id = stage['id'];
                tournament.location = 'CANTE GRIL';
                tournament.event_name = 'Circuito Cantegril ETAPA ' + stage['description'];
                tournament.category = value['description'];
                tournament.category_id = value['code'];
                tournament.tenant_id = 'teniseventos';
                tournament.groups = [];
                tournaments.push(tournament)
            })
     })
        await queryRunner.insertMany('tournament_teniseventos', tournaments);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
