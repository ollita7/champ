import { MigrationInterface, QueryRunner } from "typeorm"
import { Tournament, Group, Match, Team } from "../entities";
import { MongoQueryRunner } from "typeorm/driver/mongodb/MongoQueryRunner";
import { ObjectId } from "mongodb";

export class addStatusTournament1676402551510 implements MigrationInterface {

    public async up(queryRunner: MongoQueryRunner): Promise<void> {
        await queryRunner.updateMany('tournament',  {'tenant_id': 1}, {'$set': {'active': true}});       
    }

    public async down(queryRunner: MongoQueryRunner): Promise<void> {
    }

}
