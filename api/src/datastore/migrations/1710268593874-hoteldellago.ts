import { MigrationInterface, QueryRunner } from "typeorm"
import { Tournament, Group, Match, Team } from "../entities";
import { MongoQueryRunner } from "typeorm/driver/mongodb/MongoQueryRunner";
import { ObjectId } from "mongodb";

export class Hoteldellago1710268593874 implements MigrationInterface {

    public async up(queryRunner: MongoQueryRunner): Promise<void> {
        let stages: any = [{id: 'HDL12024', description: '1 - 23 - 24 Marzo', date: new Date(2024,2,23)},      
        {id: 'HDL2024', description: '2 - 20 - 21 Abril', date: new Date(2024,3,20)},
        {id: 'HDL32024', description: '3 - 18 - 19 Mayo', date: new Date(2024,4,18)},
        {id: 'HDL42024', description: '4 - 5-6-7 Julio ', date: new Date(2024,6,5)},
        {id: 'HDL52024', description: '5 - 17-18 Agosto', date: new Date(2024,7,17)},
        {id: 'HDL62024', description: '6 - 14 - 15 Septiembre', date: new Date(2024,8,14)},
        {id: 'HDL72024', description: '7 - 8-9-10 Noviembre (MASTER / OPEN)', date: new Date(2024,10,8)}];

        let categories: any = [{code: 'MA', description: 'MASCULINO A'}, 
        {code: 'MB', description: 'MASCULINO B'},
        {code: 'MC', description: 'MASCULINO C'},
        {code: 'FA', description: 'FEMENINO A'}, 
        {code: 'FB', description: 'FEMENINO B'},
        {code: 'FC', description: 'FEMENINO C'},
        {code: 'DMA', description: 'DOBLES MASCULINO A'}, 
        {code: 'DMB', description: 'DOBLES MASCULINO B'},
        {code: 'DMC', description: 'DOBLES MASCULINO C'},
        {code: 'DFA', description: 'DOBLES FEMENINO A'}, 
        {code: 'DFB', description: 'DOBLES FEMENINO B'},
        {code: 'DFC', description: 'DOBLES FEMENINO C'},
        {code: 'MXA', description: 'MIXTO A'}, 
        {code: 'MXB', description: 'MIXTO B'},
        {code: 'MXC', description: 'MIXTO C'}];
    
        let tournaments: any = [];
        stages.forEach(function(stage: any){
            categories.forEach(function(value: any){
                let tournament = new Tournament();        
                tournament._id = new ObjectId();
                tournament.date = stage['date'];
                tournament.event_id = stage['id'];
                tournament.location = 'HOTEL DEL LAGO';
                tournament.event_name = 'HOTEL DEL LAGO ETAPA ' + stage['description'];
                tournament.category = value['description'];
                tournament.category_id = value['code'];
                tournament.tenant_id = 'teniseventos';
                tournament.groups = [];
                tournaments.push(tournament)
            })
     })
        await queryRunner.insertMany('tournament_teniseventos', tournaments);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
