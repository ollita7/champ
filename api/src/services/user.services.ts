let jwt = require("jwt-simple");

import { getRepository } from "../datastore";
import { environment } from "../../environments/environment";
import { User } from "../datastore/entities";
import { LoginIn } from "models/user.model";
import { UserProfile } from "models/generic.model";
import { generateToken } from "../helpers/jwt";
const crypto = require("crypto");

export class UserService {

  getProfile(authHeader: string): UserProfile | null {
    try {
      const token = authHeader.replace("Bearer ", "");
      const profile = jwt.decode(token, process.env.JWT_SECRET) as UserProfile;
      return {
        _id: profile._id,
        name: profile.name,
        email: profile.email,
        password: profile.password,
        picture: profile.picture,
        role: profile.role,
        tenant_id: profile.tenant_id,
      };
    } catch (ex) {
      console.log(`getProfile error: ${ex}`);
      return null;
    }
  }

  async logIn(payload: LoginIn) {
    try {
      let repo = await getRepository(User);
      let user = await repo.findOne({ where: { email: payload.username } });
      if (!user) {
        return null;
      }
      var password = payload.password;
      var hash = crypto
        .pbkdf2Sync(password, process.env.JWT_SECRET, 1000, 64, "sha512")
        .toString("hex");
      if (hash == user.password) {
        const token = await generateToken({
          _id: user._id,
          email: user.email,
          name: user.name,
          role: user.role,
          tenant_id: user.tenant_id,
        });
        const { _id, email, name, role, tenant_id } = user;
        return { token, _id, user: { email, name, role, tenant_id } };
      } else {
        return null;
      }
    } catch (ex) {
      console.log(`login error: ${ex}`);
      return null;
    }
  }

  async getUser(name: string) {
    try {
      //check user and password
      let repo = await getRepository(User);
      let user = await repo.findOne({ where: { name } });
      delete user.password;
      return user;
    } catch (ex) {
      console.log(`get user error: ${ex}`);
      return null;
    }
  }
}
