let jwt = require("jwt-simple");
import { S3Client, ListObjectsV2Command } from "@aws-sdk/client-s3";
import { ObjectId } from "mongodb";
import { UserProfile } from "models/generic.model";
import { getRepository } from "../datastore";
import { Tenant } from "../datastore/entities";
import { TenantIn } from "models/tenant.model";
import { DeepPartial } from "typeorm";
import { merge } from "lodash";

export class TenantService {
  private s3: S3Client;

  constructor() {
    this.s3 = new S3Client({
      region: process.env.AWS_REGION,
      credentials: {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
      },
    });
  }
  async get(name: string) {
    let repo = await getRepository(Tenant);
    let tenant = await repo.findOne({ where: { name } });
    return tenant;
  }

  async getByUserTenants(userProfile: UserProfile) {
    const repo = await getRepository(Tenant);

    if (userProfile.role === "super_admin") {
      return await repo.find();
    } else if (userProfile.role === "admin") {
      const tenantIds = userProfile.tenant_id.map((id) => new ObjectId(id));
      return await repo.find({
        where: { _id: { $in: tenantIds } },
      });
    }
    return null;
  }

  public async createTenant(tenantIn: TenantIn) {
    const tenantRepo = await getRepository(Tenant);
    const result = await tenantRepo.insert(tenantIn);
    return result;
  }

  public async updateTenant(id: string, tenantData: DeepPartial<TenantIn>) {
    const tenantRepo = await getRepository(Tenant);
    const tenant = await tenantRepo.findOne({
      where: { _id: new ObjectId(id) },
    });
    if (!tenant) {
      return null;
    }
    const updatedTenant = merge({}, tenant, tenantData);
    await tenantRepo.save(updatedTenant);
    return await tenantRepo.findOne({ where: { _id: new ObjectId(id) } });
  }

  public async getSponsorImages(tenantName: string): Promise<string[]> {
    const params = {
      Bucket: "clashes",
      Prefix: `sponsors/${tenantName}/`,
    };

    try {
      const data = await this.s3.send(new ListObjectsV2Command(params));
      const images: string[] = [];
      if (data.Contents) {
        data.Contents.forEach((item) => {
          if (item.Key && !item.Key.endsWith("/")) {
            const imageUrl = `https://${params.Bucket}.s3.${process.env.AWS_REGION}.amazonaws.com/${item.Key}`;
            images.push(imageUrl);
          }
        });
      }
      return images;
    } catch (error) {
      return [];
    }
  }
}
