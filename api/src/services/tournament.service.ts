let jwt = require("jwt-simple");

import { getRepository } from "../datastore";
import { Tournament, Match } from "../datastore/entities";
import { ObjectId } from "mongodb";
import {
  Event,
  TournamentReduced,
} from "../datastore/entities/tournament.entity";
import { Statistics } from "models/match.model";
import { MatchSoccer, ResultSoccer } from "../datastore/entities/match.entity";
const crypto = require("crypto");

export class TournamentService {
  async getTournamentCategory(
    tournament_id: string,
    category_id: string,
    tenant: string
  ) {
    let repoTournament = await getRepository(Tournament, tenant);
    let matchRepo = await getRepository(Match, tenant);
    let tournament = await repoTournament.findOne({
      where: { _id: new ObjectId(tournament_id), category_id: category_id },
    });
    let matches = await matchRepo.find({
      where: {
        tournament_id: new ObjectId(tournament_id),
        category_id: category_id,
      },
    });
    tournament["groups"].map(
      (x: any) =>
        (x["matches"] = matches.filter(
          (match: any) =>
            match["group"] == x["name"] && match["stage"] == "group"
        ))
    );
    var members = tournament["groups"].map((x: any) => x["teams"]).flat();
    members.reduce((unique: any, item: any) => {
      return unique.includes(item) ? unique : [...unique, item];
    }, []);
    tournament["has_group"] = !tournament.show_group
      ? true
      : tournament.show_group;
    tournament["members"] = members;
    return tournament;
  }

  async getMatchesByTournament(tournamentId: string, tenant: string, group?: string) {
    const matchRepo = await getRepository(Match, tenant);
    const matches = await matchRepo.find({
      where: { tournament_id: new ObjectId(tournamentId), group: group },
    });
    return matches;
  }

  async getMatchesByStage(
    tournament_id: string,
    category_id: string,
    stage: string,
    group: string = null,
    tenant: string = null
  ) {
    //put stange in constants
    let matchRepo = await getRepository(Match, tenant);
    let matches = await matchRepo.find({
      where: {
        tournament_id: new ObjectId(tournament_id),
        category_id: category_id,
        stage: stage,
        group: group,
      },
    });
    return matches;
  }

  async getMatchesByDate(tournament_id: string, category_id: string) {
    //grouped by date
  }

  async getTournamentMembersByCategory(
    tournament_id: string,
    category_id: string,
    tenant: string
  ) {
    let repoTournament = await getRepository(Tournament, tenant);
    let tournament = await repoTournament.findOne({
      where: { _id: new ObjectId(tournament_id), category_id: category_id },
    });
    var members = tournament["groups"].map((x: any) => x["teams"]).flat();
    members.reduce((unique: any, item: any) => {
      return unique.includes(item) ? unique : [...unique, item];
    }, []);

    return members;
  }

  async getTournamentsByTenant(
    tenant_id: string,
    event_id: string,
    tenant: string
  ) {
    let repoTournament = await getRepository(Tournament, tenant);
    let condition: any = { tenant_id: tenant_id };
    let order: any = { date: "ASC" };
    if (event_id) {
      condition["event_id"] = event_id;
      order = { category: "DESC" };
    }

    let tournaments = await repoTournament.find({
      where: condition,
      order: order,
    });
    const events: Event[] = [];
    tournaments.map((tournament: Tournament) => {
      let event: Event = events.find((gt) => gt.id == tournament.event_id);
      if (!event) {
        event = {
          id: tournament.event_id,
          name: tournament.event_name,
          date: tournament.date,
          sport_type: tournament.sport_type,
          description: tournament.event_description,
          categories: [],
          current: false,
        };
        events.push(event);
      }
      var members = tournament["groups"].map((x: any) => x["teams"]).flat();
      members.reduce((unique: any, item: any) => {
        return unique.includes(item) ? unique : [...unique, item];
      }, []);
      if (
        members.findIndex(
          (item) => item.id && item.id.toUpperCase() == "BYE"
        ) >= 0
      ) {
        members = members.filter(
          (item) => item.id && item.id.toUpperCase() != "BYE"
        );
        members.push({ id: "BYE", name: "BYE" });
      }
      //members.forEach((x: any) => x['name'] = !x['name'] ? x['name'] : x['name'].toUpperCase())
      const reducedTournament: TournamentReduced = {
        tournament_id: tournament._id.toString(),
        category_id: tournament.category_id,
        category: tournament.category,
        cups: tournament.cups,
        stages: tournament.stages,
        sport_type: tournament.sport_type ? tournament.sport_type : "tennis",
        members: members,
        date: tournament.date,
        show_group:
          tournament.show_group === undefined ? true : tournament.show_group,
      };
      if (tournament.current) event.current = true;
      event.categories.unshift(reducedTournament);
    });
    if (event_id) return events[0].categories;
    return events;
  }

  async getStatistics(
    tournament_id: string,
    category_id: string,
    tenant: string
  ) {
    let tournamentRepo = await getRepository(Tournament, tenant);
    let tournament = await tournamentRepo.findOne({
      where: { _id: new ObjectId(tournament_id), category_id: category_id },
    });
    let matchRepo = await getRepository(MatchSoccer, tenant);
    let matches = await matchRepo.find({
      where: {
        tournament_id: new ObjectId(tournament_id),
        category_id: category_id,
      },
    });
    const teams: Statistics[] = [];
    matches.map((x: MatchSoccer) => {
      if (
        x.team_local.name == "BYE" ||
        x.team_local.name == "TBD" ||
        x.team_visit.name == "BYE" ||
        x.team_visit.name == "TBD"
      ) {
        return;
      }
      if (teams.findIndex((item) => item.team == x.team_local.name) < 0) {
        teams.push({ team: x.team_local.name, goals: 0, goals_conceded: 0 });
      }
      if (teams.findIndex((item) => item.team == x.team_visit.name) < 0) {
        teams.push({ team: x.team_visit.name, goals: 0, goals_conceded: 0 });
      }
      const result = <ResultSoccer>x.result;
      if (result) {
        let index = teams.findIndex((item) => item.team == x.team_local.name);
        if (index >= 0) {
          teams[index].goals += result.goals_local;
          teams[index].goals_conceded += result.goals_visit;
        }
        index = teams.findIndex((item) => item.team == x.team_visit.name);
        if (index >= 0) {
          teams[index].goals += result.goals_visit;
          teams[index].goals_conceded += result.goals_local;
        }
      }
    });
    const result = {
      goals: teams
        .sort((a, b) => a.goals - b.goals)
        .reverse()
        .map((x) => {
          return { team: x.team, goals: x.goals };
        }),
      goals_conceded: teams
        .sort((a, b) => a.goals_conceded - b.goals_conceded)
        .map((x) => {
          return { team: x.team, goals: x.goals_conceded };
        })
        .filter(
          (x) => x.team != "deportivo zaldi" && x.team != "El gallo sub 21 fc"
        ),
      strikers: !tournament.statistics ? [] : tournament.statistics?.strikers,
    };
    return result;
  }

  private mapTournamentToReduced(tournament: Tournament): TournamentReduced {
    const members = tournament.groups
      .map((x) => x.teams)
      .flat()
      .reduce(
        (unique, item) => (unique.includes(item) ? unique : [...unique, item]),
        []
      );

    return {
      tournament_id: tournament._id.toString(),
      date: tournament.date,
      category_id: tournament.category_id,
      category: tournament.category,
      sport_type: tournament.sport_type,
      stages: tournament.stages,
      cups: tournament.cups,
      members: members,
      show_group:
        tournament.show_group !== undefined ? tournament.show_group : true,
    };
  }
}
