let jwt = require('jwt-simple');

import { getRepository } from '../datastore';
import { Match, Team, Tournament } from '../datastore/entities'
import { ObjectId } from 'mongodb';
import { ResultIn, MatchUpdateIn, TeamIn, MatchIn, AutogenerateIn, AutogenerateQualifyingsIn } from '../models/match.model';
import { ResponseCode } from '../sdk/constants';
import { Response } from '../sdk/response';

const crypto = require("crypto")

export class MatchService {

  async updateMatchResult(match_id: string, result: ResultIn, tenant:string) {
    try {
      let matchRepo = await getRepository(Match, tenant);
      const match = await matchRepo.findOneAndUpdate(
        { _id: new ObjectId(match_id) },
        { $set: { result: result } }
      );
      match.result = result;
      return match;
    } catch (ex) {
      console.log(`update match error: ${ex}`)
      return null;
    }
  }

  async updateMatchDate(match_id: string, data: MatchUpdateIn, tenant:string) {
    try {
      let matchRepo = await getRepository(Match, tenant);
      let updateData: any = {
        date: data.date
      }
      if (data){
        updateData['team_local'] = data.team_local || { name: 'TBD' }
        updateData['team_visit'] = data.team_visit || { name: 'TBD' }
      }
      
      const match = await matchRepo.findOneAndUpdate(
        { _id: new ObjectId(match_id) },
        { $set: updateData }
      );
      match.date = data.date;
      if(data){
        match.team_local = data.team_local;
        match.team_visit = data.team_visit;
      }
      return match;
    } catch (ex) {
      console.log(`update match error: ${ex}`)
      return null;
    }
  }

  async addMatch(matchIn: MatchIn, tenant:string) {
    let matchRepo = await getRepository(Match, tenant);
    const result = await matchRepo.insert(matchIn);
    return result;
  }

  async autogenerate(data: AutogenerateIn, tenant:string) {
    let matchRepo = await getRepository(Match, tenant);
    let tournamentRepo = await getRepository(Tournament, tenant);
    const tournament = await tournamentRepo.findOne({ where: { _id: new ObjectId(data.tournament_id) } });
    if (!tournament) return null;
    const group = tournament.groups.find((g: any) => g.name == data.group);
    const new_matches = [];
    let existing_matches = await this.getMatchesByGroup(data.tournament_id, tournament.category_id, data.group, tenant);
    if (existing_matches.length > 0) {
      const matches_with_result = existing_matches.filter((match: any) => match['result']);
      if (matches_with_result.length > 0) {
        return new Response(ResponseCode.ERROR, 'Los partidos ya tienen resultados, no se pueden regenerar');
      } else {
        //delete all matches to regenerate
        return new Response(ResponseCode.ERROR, 'Ya tiene partidos generados');
        //TODO: await matchRepo.deleteMany({ where: { 'tournament_id': new ObjectId(data.tournament_id), 'category_id': tournament.category_id, 'group': group } });       
      }
    }

    if (group) {
      const teams = group.teams;
      for (var i = 0; i < teams.length; i++) {
        const team_local = teams[i];
        for (var j = i + 1; j < teams.length; j++) {
          const team_visit = teams[j];
          const match = new MatchIn();
          match.category_id = tournament.category_id;
          match.tournament_id = new ObjectId(data.tournament_id);
          match.date = 'TBD';
          match.stage = 'group';
          match.group = data.group;
          match.team_local = team_local;
          match.team_visit = team_visit;
          new_matches.push(match);
        }
      }
      await matchRepo.insert(new_matches);
      return new Response(ResponseCode.OK, 'Partidos generados');
    }

    return new Response(ResponseCode.ERROR, 'No existe el grupo');

  }

  async getMatchesByGroup(tournament_id: string, category_id: string, group: string, tenant:string) {
    let matchRepo = await getRepository(Match, tenant);
    const matches = await matchRepo.find({ where: { 'tournament_id': new ObjectId(tournament_id), 'category_id': category_id, 'group': group } });
    return matches;
  }
  async autogenerateQualifyings(data: AutogenerateQualifyingsIn, tenant:string) {
    let matchRepo = await getRepository(Match, tenant);
    let tournamentRepo = await getRepository(Tournament, tenant);
    const tournament = await tournamentRepo.findOne({ where: { _id: new ObjectId(data.tournament_id) } });
    if (!tournament) return null;

    //TODO: Check valid stage  "8F", "QF", "SF", "F"
    //TODO add default number matches by stage
    const new_matches = [];
    for (var i = 0; i < data.total; i++) {
      const match = new MatchIn();
      match.category_id = tournament.category_id;
      match.tournament_id = new ObjectId(data.tournament_id);
      match.date = 'TBD';
      match.stage = data.stage;
      match.group = data.cup;
      match.team_local = { id: "", country: "tbd", name: "TBD" };
      match.team_visit = { id: "", country: "tbd", name: "TBD" };
      new_matches.push(match);
    }

    const result = await matchRepo.insert(new_matches);
    return new Response(ResponseCode.OK, '');
  }

  private teamInToTeam(team: TeamIn) {
    var new_team = new Team();
    new_team.country = team.country;
    new_team.name = team.name;
    return new_team;
  }

  private matchInToMatch(match: MatchIn, _id: string) {
    var new_match = new Match();
    new_match._id = new ObjectId(_id);
    new_match.category_id = match.category_id;
    new_match.date = match.date;
    new_match.group = match.group;
    new_match.result = match.result;
    var team_local = new Team();
    var team_visit = new Team();
    team_local.name = match.team_local.name;
    team_local.country = match.team_local.country;
    team_visit.name = match.team_visit.name;
    team_visit.country = match.team_visit.country;
    new_match.team_local = team_local;
    new_match.team_visit = team_visit;
    new_match.tournament_id = new ObjectId(match.tournament_id);
    return new_match;
  }
}