let jwt = require("jwt-simple");

export const generateToken = (user: {
  _id: string;
  email: string;
  name: string;
  role: string;
  tenant_id: string[];
}) => {
  const tokenPayload = {
    _id: user._id,
    email: user.email,
    name: user.name,
    role: user.role,
    tenant_id: user.tenant_id,
    // exp: Math.floor(Date.now() / 1000) + 60 * 60,
    exp: Math.floor(Date.now() / 1000) + 1820000,
  };
  return jwt.encode(tokenPayload, process.env.JWT_SECRET);
};